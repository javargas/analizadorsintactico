
package javargas.parser.spanish;

import danbikel.lisp.*;
import danbikel.parser.Settings;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.text.*;

/**
 * WordFeatures are orthographic and morphological features of
 * words.  Specifically, the word features encoded by the methods of this class
 * are:
 * <ol>
 * <li>capitalization
 * <li>hyphenization
 * <li>inflection
 * <li>derivation
 * <li>numeric
 * </ol>
 * The features are encoded into a single symbol of the form:
 * <tt>CcHhIiDdNn</tt>, where <tt>c</tt> encodes capitalization, <tt>h</tt>
 * encodes hyphenization, <tt>i</tt> encodes inflection, <tt>d</tt> encodes
 * derivation and <tt>n</tt> encodes the numeric feature.  For example,
 * <tt>&quot;C3H0I0D3N0&quot;</tt> encodes the features for the word
 * <tt>&quot;Geography&quot;</tt> (that is, non-sentence-initial capitalized,
 * no hyphenization, no inflection, <tt>&quot;graphy&quot;</tt> derivation and
 * non-numeric).
 */
public class SVMFeatures extends danbikel.parser.lang.AbstractWordFeatures {

  private final static ParsePosition pos = new ParsePosition(0);
  private final static NumberFormat nf = NumberFormat.getNumberInstance();

  /**
   * The property obtained from the {@link Settings} class to indicate
   * whether or not to consider underscores when creating the feature vector.
   */
  public final static String useUnderscoresProperty =
    "parser.wordfeatures.english.useUnderscores";

  private static boolean useUnderscores =
    Settings.getBoolean(useUnderscoresProperty);

  private final static Symbol defaultFeatureVector = Symbol.add("C0H0I0D0N0");

  private static String[] derivationalFeatures = {
    "-backed", "-based", "graphy", "meter", "ente", "ment", "ness",
    "tion", "ael", "ary", "ate", "ble", "ent", "ess", "est", "ial",
    "ian", "ine", "ion", "ism", "ist", "ite", "ity", "ive", "ize",
    "nce", "ogy", "ous", "sis", "uan", "al", "an", "as", "er", "ez",
    "ia", "ic", "ly", "on", "or", "os", "um", "us", "a", "i", "o", "y"};

  private static char[] consonantChars = {
    'b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'p', 'q', 'r', 's',
    't', 'v', 'w', 'x', 'y', 'z'
  };

  private final static int featureStrLen = 10;

  private static BitSet consonants = new BitSet(Byte.MAX_VALUE);
  static {
    for (int i = 0; i < consonantChars.length; i++)
      consonants.set(consonantChars[i]);
  }

  /**
   * Constructs a new instance of this class for deterministically mapping
   * English words to word-feature vectors.
   */
  public SVMFeatures() 
  {  }

  /**
   * Returns the features of a word.
   *
   * @param word the word.
   * @param firstWord indicates whether <code>word</code> is the first word
   * of the sentence in which it occurs
   * @return the encoded feature symbol.
   */
  public Symbol features(Symbol word, boolean firstWord) {
    System.out.println("[INICIO] WordFeatures.features(word: "+word+") ");
    
    String wordStr = word.toString();
    String returnStr;

    if (isNumber(wordStr)) {
      returnStr = "1:0 2:0 3:0 4:0 5:1";
    } else {
      returnStr =
        (new StringBuffer(featureStrLen)).
                append("1:").
        append(capitalizationFeature(wordStr,
                                     firstWord)).
                append(" 2:").
        append(hyphenizationFeature(wordStr)).
                append(" 3:").
        append(inflectionalFeature(wordStr)).
                append(" 4:").
        append(derivationalFeature(wordStr)).
        append(" 5:0").
        toString();
    }
    
    System.out.println("[FIN] WordFeatures.features(returnStr: "+returnStr+") ");
    return Symbol.add(returnStr);
  }

  public Symbol defaultFeatureVector() {
    System.out.println("[INICIO] WordFeatures.defaultFeatureVector ");
    return defaultFeatureVector;
  }
  
  /**
   * Caracteristicas de complemento y adjuncion.
   * Clasificación realizada por máquinas de vectores de soporte.
   * 
   * @param word
   * @param firstWord
   * @return 
   */
  private static String complementFeature(String word, boolean firstWord)
  {
      return "";
  }
  private static String adjuntFeature(String word, boolean firstWord)
  {
      return "";
  }

  private static String capitalizationFeature(String word, boolean firstWord) {
    if (Character.isUpperCase(word.charAt(0))) {
      if (firstWord)
	return "1";
      else if (word.equals(word.toUpperCase())) {
	for (int i = 0; i < word.length(); i++) {
	  if (Character.isDigit(word.charAt(i)))
	    return "2";
	}
	return "3";
      }
      else
	return "4";
    }
    return "0";
  }

  private static String hyphenizationFeature(String word) {
    for (int i = 0; i < word.length(); i++) {
      if (word.charAt(i) == '-')
	return "1";
      else if (useUnderscores && (word.charAt(i) == '_'))
	return "2";
      else if (word.charAt(i) == '$')
	return "3";
    }
    return "0";
  }

  private static String inflectionalFeature(String word) {
    if (word.length() > 3) {
      if (sInflection(word))
	return "1";
      else if (word.endsWith("ed"))
	return "2";
      else if (word.endsWith("ing"))
	return "3";
    }
    return "0";
  }

  private static String derivationalFeature(String word) {
    if (word.length() > 6) {
      if (sInflection(word)) {
	word = word.substring(0, word.length() - 1);
      }
      for (int i = 0; i < derivationalFeatures.length; i++) {
	String feature = derivationalFeatures[i];
	if (word.endsWith(feature)) {
	  return Integer.toString(i + 1);
	}
      }
    }
    return "0";
  }

  private static boolean isNumber(String word) {
    pos.setIndex(0);
    pos.setErrorIndex(-1);
    nf.parse(word, pos);
    return pos.getIndex() == word.length() && pos.getErrorIndex() == -1;
  }

  private static boolean sInflection(String word) {
    if (word.length() > 2) {
      char lastChar = word.charAt(word.length() - 1);
      char nextToLast = word.charAt(word.length() - 2);
      return ((lastChar == 's') && (nextToLast != 's') &&
	      ((nextToLast == 'e') || isConsonant(nextToLast)));
    }
    else
      return false;
  }

  private static boolean isConsonant(char ch) {
    if ((int)ch >= Byte.MAX_VALUE)
      return false;
    else
      return consonants.get((byte)ch);
  }
}
