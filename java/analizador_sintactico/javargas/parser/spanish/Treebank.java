
package javargas.parser.spanish;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import danbikel.lisp.*;
import danbikel.parser.Nonterminal;
import danbikel.parser.Settings;

/* *         
 * Representa la estrucutra para Ancora corpus.
 *
 * <a href="http://clic.ub.edu/corpus/">Ancora Corpus</a>
 */
public class Treebank extends danbikel.parser.lang.AbstractTreebank {
    
 // Arreglo de cadenas para guardar los lemas */   
 private static HashMap<String, String> lemas = new HashMap<String, String>();
 
  // the characters that are delimiters of augmented nonterminal labels
  private final static String augmentationDelimStr = "-=|";

  private final static boolean outputLexLabels =
    Settings.getBoolean(Settings.decoderOutputHeadLexicalizedLabels);

  // basic nodes in the Spanish Treebank that will be transformed in a
  // preprocessing phase
  /* NP: Noun Phrase, que en ancora seria el sintagma nominal. */
  static final Symbol NP = Symbol.add("sn");
  
  /* En ambos corpus S representa la sentencia */
  static final Symbol S = Symbol.add("S");
  
  // transformed nodes
  //static final Symbol baseNP = Symbol.add(NP + "B");
  static final Symbol baseNP = Symbol.add("grup.nom");
  static final Symbol baseNPArg = Symbol.add(baseNP + "");
  static final Symbol subjectlessS = Symbol.add(NP + "-CC");
  static final Symbol subjectAugmentation = Symbol.add(NP + "-SUJ");

  // other basic nodes
  static final Symbol WHNP = Symbol.add("sadv");
  static final Symbol SP = Symbol.add("sp");
  static final Symbol GV = Symbol.add("gv");
  static final Symbol CC = Symbol.add("cc");
  static final Symbol comma = Symbol.add(",");
  static final Symbol lrb = Symbol.add("-LRB-");
  static final Symbol lcb = Symbol.add("-LCB-");
  static final Symbol rrb = Symbol.add("-RRB-");
  static final Symbol rcb = Symbol.add("-RCB-");

  // null element
  static final Symbol nullElementPreterminal = Symbol.add("-NONE-");
  // possessive part of speech
  static final Symbol possessivePos = Symbol.add("espec");

  // exception nonterminals for defaultParseNonterminal
  static final Symbol[] nonterminalExceptionArr = {lrb, rrb};

  private static Symbol[][] canonicalLabelMapData = {
    {baseNP, NP},
    {subjectlessS, S},
  };
  private static HashMap canonicalLabelMap =
    new HashMap(canonicalLabelMapData.length);
  static {
    for (int i = 0; i < canonicalLabelMapData.length; i++)
      canonicalLabelMap.put(canonicalLabelMapData[i][0],
                            canonicalLabelMapData[i][1]);
  }

  private static String[] puncToRaiseElements = {",", ":"};

  private static Set puncToRaise = new HashSet();
  static {
    for (int i = 0; i < puncToRaiseElements.length; i++)
      puncToRaise.add(Symbol.add(puncToRaiseElements[i]));
  }

  private static String[] verbTagStrings = {"grup.verb", "vmip3s0", "vmsp1s0", 
                            "vmis3s0", "vaip3s0", "vsis3s0"};
  private static Set verbTags = new HashSet();
  static {
    for (int i = 0; i < verbTagStrings.length; i++)
      verbTags.add(Symbol.add(verbTagStrings[i]));
  }

  /**
   * Constructs an English <code>Treebank</code> object.
   */
  public Treebank() {
    super();  System.out.println("XXXXXXXX  TreeBank para español [Modified 20140211] XXXXXXXXXX");
    nonterminalExceptionSet = nonterminalExceptionArr;
  }

  /**
   * Returns <code>true</code> if <code>tree</code> represents a preterminal
   * subtree (part-of-speech tag and word).  Specifically, this method
   * returns <code>true</code> if <code>tree</code> is an instance of
   * <code>SexpList</code>, has a length of 2 and has a first list element
   * of type <code>Symbol</code>.
   * 
   * Se modifica para permirtir nodos con 3 etiquetas:
           - Etiqueta (Tag)
           - Palabra (Forma)
           - Lema (Morfología) 
     **/
  public final boolean isPreterminal(Sexp tree) {      
      boolean isNoTerminal = tree.isList() 
              && tree.list().length() == 2
              && tree.list().get(0).isSymbol()
              && tree.list().get(1).isSymbol();// Etiqueta (Tag)
	    
      
      boolean isTerminal = tree.isList() 
              && tree.list().length() == 3 
              && tree.list().get(0).isSymbol()  // Etiqueta (Tag)
              && tree.list().get(1).isSymbol() // Palabra (forma)
	      && tree.list().get(2).isSymbol();
      
      if (isTerminal) {
        /* Guardando lema en un hashmap */
        lemas.put((tree.list().get(1)).toString(), (tree.list().get(2)).toString());
      }
    return (isTerminal || isNoTerminal);
  }

  /**
   * Este método evita adicionar la aumentación (-A) en el marco de 
   * subcategorización.
   * 
   * @param nonterminal
   * @param augmentation 
   */
    public void addAugmentation(Nonterminal nonterminal,
			      Symbol augmentation) {
    return;
  }
  
  /**
   * Returns <code>true</code> is the specified nonterminal label represents a
   * sentence in the Penn Treebank, that is, if the canonical version of
   * <code>label</code> is equal to <code>&quot;S&quot;</code>.
   * @see Training#relabelSubjectlessSentences(Sexp)
   */
  public boolean isSentence(Symbol label) { return getCanonical(label) == S; }

  public Symbol sentenceLabel() { return S; }

  /**
   * Returns the symbol that
   * {@link Training#relabelSubjectlessSentences(Sexp)}
   * will use for sentences that have no subjects.
   */
  public Symbol subjectlessSentenceLabel() { return subjectlessS; }

  public Symbol subjectAugmentation() { return subjectAugmentation; }

  /**
   * Returns <code>true</code> if the specified S-expression represents a
   * preterminal whose terminal element is the null element
   * (<code>&quot;-NONE-&quot;</code>) for the Penn Treebank.
   * @see Training#relabelSubjectlessSentences(Sexp)
   */
  public boolean isNullElementPreterminal(Sexp tree) {
    return (isPreterminal(tree) &&
	    tree.list().get(0).symbol() == nullElementPreterminal);
  }

  /**
   * Returns <code>true</code> if the specified S-expression is a preterminal
   * whose part of speech is <code>&quot;,&quot;</code> or
   * <code>&quot;:&quot;</code>.
   */
  public boolean isPuncToRaise(Sexp preterm) {
    return (isPreterminal(preterm) &&
	    puncToRaise.contains(preterm.list().first()));
  }

  public boolean isPunctuation(Symbol tag) {
    //return puncToRaise.contains(stripAugmentation(tag));
    return puncToRaise.contains(tag);
  }

  /**
   * Returns <code>true</code> if the specified S-expression represents
   * a preterminal that is the possessive part of speech.  This method is
   * intended to be used by implementations of {@link
   * Training#addBaseNPs(Sexp)}.
   */
  public boolean isPossessivePreterminal(Sexp tree) {
    return (isPreterminal(tree) &&
	    tree.list().get(0).symbol() == possessivePos);
  }

  /**
   * Returns <code>true</code> if the canonical version of the specified label
   * is an NP for for English Treebank.
   *
   * @see Training#addBaseNPs(Sexp)
   */
  public boolean isNP(Symbol label) {
    return getCanonical(label) == NP;
  }

  public boolean isBaseNP(Symbol label) {
    return label == baseNP || label == baseNPArg;
  }

  /**
   * Returns the symbol with which {@link Training#addBaseNPs(Sexp)} will
   * relabel base NPs.
   *
   * @see Training#addBaseNPs
   */
  public Symbol baseNPLabel() { return baseNP; }

  /**
   * Returns <code>true</code> if the canonical version of the specified label
   * is a WHNP in the English Treebank.
   *
   * @see Training#addGapInformation(Sexp)
   */
  public boolean isWHNP(Symbol label) { return getCanonical(label) == WHNP; }

  /**
   * Returns the symbol that {@link Training#addBaseNPs(Sexp)} should
   * add as a parent if a base NP is not dominated by an NP.
   */
  public Symbol NPLabel() { return NP; }

  /**
   * Returns <code>true</code> if <code>label</code> is equal to the symbol
   * whose print name is <code>&quot;CC&quot;</code>.
   */
  public boolean isConjunction(Symbol label) {
    //return getCanonical(label) == CC;
    return label == CC;
  }

  /**
   * Returns <code>true</code> if <code>preterminal</code> represents a
   * terminal with one of the following parts of speech: <tt>VB, VBD, VBG,
   * VBN, VBP</tt> or <tt>VBZ</tt>.  It is an error to call this method
   * with a <code>Sexp</code> object for which {@link #isPreterminal(Sexp)}
   * returns <code>false</code>.<br>
   *
   * @param preterminal the preterminal to test
   * @return <code>true</code> if <code>preterminal</code> is a verb
   */
  public boolean isVerb(Sexp preterminal) {
    return isVerbTag(preterminal.list().get(0).symbol());
  }

  public boolean isVerbTag(Symbol tag) {
    return verbTags.contains(tag);
  }

  public boolean isComma(Symbol word) {
    return word == comma;
  }

  public boolean isLeftParen(Symbol word) {
    return word == lrb || word == lcb;
  }

  public boolean isRightParen(Symbol word) {
    return word == rrb || word == rcb;
  }

  /**
   * Returns a canonical mapping for the specified nonterminal label; if
   * <code>label</code> already is in canonical form, it is returned.
   * The canonical mapping refers to transformations performed on nonterminals
   * during the training process.  Before obtaining a label's canonical form,
   * it is also stripped of all Treebank augmentations, meaning that only
   * the characters before the first occurrence of '-', '=' or '|' are kept.
   *
   * @return a <code>Symbol</code> with the same print name as
   * <code>label</code>, except that all training transformations and Treebank
   * augmentations have been undone and stripped
   */
  public final Symbol getCanonical(Symbol label) {
      
  //    System.out.println(" XXX => getCanonical label: " + label);
    if (outputLexLabels) {
      char lbracket = nonTreebankLeftBracket();
      char rbracket = nonTreebankRightBracket();
      int lbracketIdx = label.toString().indexOf(lbracket);
      int rbracketIdx = label.toString().indexOf(rbracket);
      if (lbracketIdx != -1 && rbracketIdx != -1) {
	String labelStr = label.toString();
     //   System.out.println("XXX => labeStr: " + labelStr);
        
	Symbol unlexLabel = Symbol.get(labelStr.substring(0, lbracketIdx) +
				       labelStr.substring(rbracketIdx + 1));
	String canonStr = defaultGetCanonical(unlexLabel).toString();
     //   System.out.println("XXX => canonStr: " + canonStr);
	return Symbol.get(canonStr +
			  labelStr.substring(lbracketIdx, rbracketIdx + 1));
      }
    }
    return defaultGetCanonical(label);
  }

  private Symbol defaultGetCanonical(Symbol label) {
    for (int i = 0; i < nonterminalExceptionSet.length; i++)
      if (label == nonterminalExceptionSet[i])
	return label;
    Symbol strippedLabel = stripAugmentation(label);
    Symbol mapEntry = (Symbol)canonicalLabelMap.get(strippedLabel);
    return ((mapEntry == null) ? strippedLabel : mapEntry);
  }

  public final Symbol getCanonical(Symbol label, boolean stripAugmentations) {
    if (stripAugmentations)
      return getCanonical(label);
    else {
      Symbol mapEntry = (Symbol)canonicalLabelMap.get(label);
      return (mapEntry == null ? label : mapEntry);
    }
  }

  /**
   * Calls {@link Treebank#defaultParseNonterminal(Symbol, Nonterminal)} with
   * the specified arguments.
   * @param label to the nonterminal label to parse
   * @param nonterminal the <code>Nonterminal</code> object to fill with
   * the components of <code>label</code>
   */
  public Nonterminal parseNonterminal(Symbol label, Nonterminal nonterminal) {
      
  //    System.out.println("XXX=> parseNonterminal label: " + label);
  //    System.out.println("XXX=> parseNonterminal nonterminal: " + nonterminal);
    defaultParseNonterminal(label, nonterminal);
    return nonterminal;
  }

  /**
   * Returns a string of the three characters that serve as augmentation
   * delimiters in the Penn Treebank: <code>&quot;-=|&quot;</code>.
   */
  public String augmentationDelimiters() { return augmentationDelimStr; }
}
