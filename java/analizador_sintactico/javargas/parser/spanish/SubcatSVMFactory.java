/* *           Copyright (c) 2004, Daniel M. Bikel.
 *                         All rights reserved.
 * 
 *                Developed at the University of Pennsylvania
 *                Institute for Research in Cognitive Science
 *                    3401 Walnut Street
 *                    Philadelphia, Pennsylvania 19104
 * 			
 * 
 * For research or educational purposes only.  Do not redistribute.  For
 * complete license details, please read the file LICENSE that accompanied
 * this software.
 * 
 * DISCLAIMER
 * 
 * Daniel M. Bikel makes no representations or warranties about the suitability of
 * the Software, either express or implied, including but not limited to the
 * implied warranties of merchantability, fitness for a particular purpose, or
 * non-infringement. Daniel M. Bikel shall not be liable for any damages suffered
 * by Licensee as a result of using, modifying or distributing the Software or its
 * derivatives.
 * 
 */
    package javargas.parser.spanish;

import danbikel.lisp.*;
import danbikel.parser.Subcat;
import danbikel.parser.SubcatFactory;

/**
 * A factory for creating <code>SubcatBag</code> objects.
 *
 * @see SubcatBag
 * @see Subcats
 * @see Settings#subcatFactoryClass
 */
public class SubcatSVMFactory implements SubcatFactory {
  /** Constructs a new <code>SubcatBagFactory</code>. */
  public SubcatSVMFactory() {}

  /** Returns an empty <code>SubcatBag</code>.
     * @return  */
  @Override
  public Subcat get() { return new SubcatSVM(); }

  /**
   * Returns a <code>SubcatBag</code> initialized with the requirements
   * contained in the specified list.
   *
   * @param list a list of <code>Symbol</code> objects to be added
   * as requirements to a new <code>SubcatBag</code> instance
     * @return 
   */
  @Override
  public Subcat get(SexpList list) { return new SubcatSVM(list); }
}
