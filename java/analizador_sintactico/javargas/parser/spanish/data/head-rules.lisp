; Archivo con reglas para encontrar el nucleo sintactico de 
; un constituyente o sintagma (head)
;
; Autor: John Vargas <javargas@gmail.com>
; Fecha: Abril 1 de 2014
;
(
 (grup.verb (r infinitiu) (r gerundi) (r vmp) (r vsp) (r vap) (r vmi))
 (S (r grup.verb) (r S) (r sn) )
 (sn (r grup.nom))
 (sp (l prep))
 (* (r))
)
