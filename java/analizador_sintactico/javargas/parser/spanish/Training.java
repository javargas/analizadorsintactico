/** 
 * 
 */
package javargas.parser.spanish;

import danbikel.lisp.*;
import danbikel.parser.Constants;
import danbikel.parser.Language;
import danbikel.parser.Nonterminal;
import danbikel.parser.Settings;
import java.io.*;
import java.util.*;
import javargas.parser.svm.Clasificador;
import javargas.parser.svm.ExtractorCaracteristicas;

/**
 * Provides methods for language-specific processing of training parse trees.
 * Even though this subclass of {@link danbikel.parser.Training} is
 * in the default English language package, its primary purpose is simply
 * to fill in the {@link #argContexts}, {@link #semTagArgStopSet} and
 * {@link #nodesToPrune} data members using a metadata resource.  If this
 * capability is desired in another language package, this class may be
 * subclassed.
 * <p>
 * This class also re-defined the method
 * {@link danbikel.parser.Training#addBaseNPs(Sexp)}, with an important change
 * that is possibly only relevant to the Penn Treebank.
 */
public class Training extends danbikel.parser.lang.AbstractTraining {
  // constants
  private final static String className = Training.class.getName();
  private final static Symbol VP = Symbol.get("VP");
  
  private static int numCC = 0, numArg = 0, numNeg = 0;
  
  // data members
  private Nonterminal nonterminal = new Nonterminal();
  
  private Nonterminal nonterminal2 = new Nonterminal();
  
  // Tipo de Argumento
  private String tmpTipoArgumento = "";  

  /**
   * The default constructor, to be invoked by {@link danbikel.parser.Language}.
   * This constructor looks for a resource named by the property
   * <code>metadataPropertyPrefix + language</code>
   * where <code>metadataPropertyPrefix</code> is the value of
   * the constant {@link #metadataPropertyPrefix} and <code>language</code>
   * is the value of <code>Settings.get(Settings.language)</code>.
   * For example, the property for English is
   * <code>&quot;parser.training.metadata.english&quot;</code>.
   */
  public Training() throws FileNotFoundException, IOException {
    //defaultArgAugmentation = Symbol.add("C");      
      
    String language = Settings.get(Settings.language);
    String metadataResource = Settings.get(metadataPropertyPrefix + language);
    InputStream is = Settings.getFileOrResourceAsStream(this.getClass(),
							metadataResource);
    int bufSize = Constants.defaultFileBufsize;
    SexpTokenizer metadataTok =
      new SexpTokenizer(is, Language.encoding(), bufSize);
    readMetadata(metadataTok);
  }

  /**
   * Strips any augmentations off all of the nonterminal labels of
   * <code>tree</code>.  The set of nonterminal labels does <i>not</i> include
   * preterminals, which are typically parts of speech.  If a particular
   * language's Treebank augments preterminals, this method should be
   * overridden in a language package's subclass. The only augmentations that
   * will not be removed are those that are added by {@link
   * #identifyArguments(Sexp)}, so as to preserve the transformations of that
   * method.  This method should only be called subsequent to the invocations
   * of methods that require augmentations, such as {@link
   * #relabelSubjectlessSentences(Sexp)}.
   *
   * @param tree the tree all of the nonterminals of which are to be stripped
   * of all augmentations except those added by <code>identifyArguments</code>
   * @return a reference to <code>tree</code>
   */
  @Override
  public Sexp stripAugmentations(Sexp tree) {
      return stripAugmentations(tree, null);
  }
  
  public Sexp stripAugmentations(Sexp tree, Sexp objNodoPadre) {
    ExtractorCaracteristicas extractor = new ExtractorCaracteristicas();
    boolean blnContinue = true;
    
    if (tree.isList()) {
        
        Symbol label = tree.list().first().symbol();

        /* javargas:2014-Sep-12 Se guardan las caracteristicas en un archivo 
         * que potencialmente seria la entrada del componente libSVM
        */
        tmpTipoArgumento = "";
        Symbol augmentedLabel = stripAugmentations(label);      
        String  argument = tmpTipoArgumento; 
      
        System.out.println(" Arbol para analizar caracteristicas: " + tree.toString());
        
        try {
            File file = new File("archivo_caracteristicas.txt");
            BufferedWriter output = new BufferedWriter(new FileWriter(file, true));                               
            String clase = "0";          
            
            
            /* Identificación de adjuntos */
            if (argument.equals("CC")) {
                clase = "1";
            }
            
            /* Identificación de adjuntos */
            if (argument.equals("SUJ")) {
                clase = "2";
            }
            
            /* Identificación de adjuntos */
            if (argument.equals("CD")) {
                clase = "3";
            }
            
            /* Identificación de adjuntos */
            if (argument.equals("CI")) {
                clase = "4";
            }
            
            /* Identificación de adjuntos */
            if (argument.equals("CPRED")) {
                clase = "5";
            }
            
            /* Identificación de adjuntos */
            if (argument.equals("CREG")) {
                clase = "6";
            }
            
            /* Identificación de adjuntos */
            if (argument.equals("CAG")) {
                clase = "7";
            }
            
            /* Identificación de adjuntos */
            if (argument.equals("MOD")) {
                clase = "8";
            }
            
            /* Identificación de adjuntos */
            if (argument.equals("ATR")) {
                clase = "9";
            }
            
            /* Identificación de adjuntos */
            if (argument.equals("CC")) {
                clase = "1";
            }
            
            /* -1: complemento, 0: ninguno, 1: adjunto */
            String vectorCaracteristicas = extractor.obtenerCaracteristicas(tree, objNodoPadre);
            
            if (!clase.equals("0")) {
                //output.append(clase+" "+vectorCaracteristicas+" -> (argument) "+argument+" : (tree) " + tree.toString() + "\n");  
                output.append(clase+" "+vectorCaracteristicas + "\n");               
            }
            output.close();
            
        } catch ( IOException e ) {
           //e.printStackTrace();
        }
        /* Fin javargas */
      
        tree.list().set(0, augmentedLabel);
        int treeListLen = tree.list().length();
        
        for (int i = 1; i < treeListLen; i++) {
            stripAugmentations(tree.list().get(i), tree);
        }
    }
    
    return tree;
  }
   
  /**
   * Parses the specified nonterminal label and removes all augmentations.
   *
   * @param label the label from which to strip all augmentations
   * @return the specified label having been stripped of augmentations
   */
  @Override
  protected Symbol stripAugmentations(Symbol label) {
    stripAugmentations(label, nonterminal, true);
    return nonterminal.toSymbol();
  }
  
  
  @Override
  protected void stripAugmentations(Symbol label, Nonterminal nonterminal,
                                    boolean parseLabel) 
  {
    if (parseLabel)
      treebank.parseNonterminal(label, nonterminal);
    SexpList augmentations = nonterminal.augmentations;
        
    for (int i = 0; i < augmentations.length(); i++) {
      Sexp thisAug = augmentations.get(i);
      if (!Language.treebank().isAugDelim(thisAug) &&
          thisAug != gapAugmentation &&
          !argAugmentations.contains(thisAug)) {
        i--; // move index to delimeter that preceded this arg augmentation
        augmentations.remove(i);  // remove delimeter
        augmentations.remove(i);  // remove arg augmentation
        
        tmpTipoArgumento = thisAug.toString();

        i--; // decrement index again to offset increment in for loop
      }  
    }
    
    // if there's an index, make sure to strip delimeter that precedes it
    if (nonterminal.index >= 0)
      augmentations.remove(augmentations.size() - 1);
    nonterminal.index = -1; // effectively strips off index
  }
  
  /**
   * Augments labels of nonterminals that are arguments.  This method is
   * optional, and may be overridden to simply return <code>tree</code>
   * untouched if argument identification is not desired for a particular
   * language package.
   * <p>
   * Note that children in a coordinated phrase are never relabeled as
   * arguments, as determined by subtrees for which
   * {@link #isCoordinatedPhrase(Sexp,int)} returns <code>true</code>.
   *
   * @param tree the parse tree to modify
   * @return a reference to the modified <code>tree</code> object
   * @see Treebank#canonicalAugDelimiter
   */
  @Override
  public Sexp identifyArguments(Sexp tree) {

    if (treebank.isPreterminal(tree)) {
      return tree;
    }
    if (tree.isList()) {
      SexpList treeList = tree.list();
      int treeListLen = treeList.length();

      // first, make recursive call if not already at max recursion level
      for (int childIdx = 1; childIdx < treeListLen; childIdx++)
	identifyArguments(treeList.get(childIdx));

      // collect *all* candidate pattern lists in argContexts that apply to the
      // current parent: run through each key in argContexts map, and see if it
      // subsumes current parent; if so, add to allCandidatePatterns, which is
      // a list of lists
      Symbol parent = tree.list().first().symbol();
      Nonterminal parsedParent =
	treebank.parseNonterminal(parent, nonterminal);
      SexpList allCandidatePatterns = new SexpList();
      Iterator entries = argContexts.entrySet().iterator();
      while (entries.hasNext()) {
	Map.Entry entry = (Map.Entry)entries.next();
	Symbol key = (Symbol)entry.getKey();
	SexpList currPatternList = (SexpList)entry.getValue();
	Nonterminal parsedKey = treebank.parseNonterminal(key, nonterminal2);
	if (parsedKey.subsumes(parsedParent))
	  allCandidatePatterns.add(currPatternList);
      }

      int headIdx = headFinder.findHead(treeList);
      
      for (int patternIdx = 0; patternIdx < allCandidatePatterns.length();
	   patternIdx++) {
	SexpList candidatePatterns = allCandidatePatterns.listAt(patternIdx);

	Symbol headChild = (headIdx == 0 ? null :
			    treeList.get(headIdx).list().first().symbol());

	if (isCoordinatedPhrase(tree, headIdx)) {            
	  return tree;
        }

	// either the candidate pattern list has the form (head <int>) or
	// (head-left <list>) or (head-right <list>) or it's a list of actual
	// nonterminal labels
	if (candidatePatterns.first().symbol() == headSym ||
	    candidatePatterns.first().symbol() == headPreSym ||
	    candidatePatterns.first().symbol() == headPostSym) {
	  int argIdx = -1;
	  Symbol child = null;
	  boolean foundArg = false;

	  if (candidatePatterns.first().symbol() == headSym) {
	    String offsetStr = candidatePatterns.get(1).toString();
	    int headOffset = Integer.parseInt(offsetStr);
	    // IF there is a head and IF that head is not equal to parent (i.e.,
	    // if it's not a situation like (PP (PP ...) (CC and) (PP ...)) ) and
	    // if the headIdx plus the headOffset is still valid, then we've got
	    // an argument
	    argIdx = headIdx + headOffset;
	    if (headIdx > 0 &&
		treebank.getCanonical(headChild) != parent &&
		argIdx > 0 && argIdx < treeListLen) {
	      child = treeList.getChildLabel(argIdx);

	      // if the arg is actually a conjunction or punctuation,
	      // OR ANY KIND OF PRETERMINAL,
	      // if possible, find the first child to the left or right of
	      // argIdx that is not a preterminal
	      if (treebank.isPreterminal(child) ||
		  treebank.isConjunction(child) || treebank.isPunctuation(child)) {
		if (headOffset > 0) {
		  for (int i = argIdx + 1; i < treeListLen; i++) {
		    if (!treebank.isPreterminal(treeList.get(i))) {
		      argIdx = i;
		      child = treeList.getChildLabel(argIdx);
		      break;
		    }
		  }
		}
		else {
		  for (int i = argIdx - 1; i >= 0; i--) {
		    if (!treebank.isPreterminal(treeList.get(i))) {
		      argIdx = i;
		      child = treeList.getChildLabel(argIdx);
		      break;
		    }
		  }
		}
	      }
	      foundArg = !treebank.isPreterminal(treeList.get(argIdx));
	    }
	  }
	  else {
	    SexpList searchInstruction = candidatePatterns;
	    int searchInstructionLen = searchInstruction.length();
	    boolean leftSide =
	      searchInstruction.symbolAt(0) == headPreSym;
	    boolean leftToRight =
	      searchInstruction.symbolAt(1) == Constants.firstSym;
	    boolean negativeSearch =
	      searchInstruction.symbolAt(2) == Constants.notSym;
	    int searchSetStartIdx = negativeSearch ? 3 : 2;
	    if (headIdx > 0 && treebank.getCanonical(headChild) != parent) {
	    //if (headIdx > 0) {
	      int increment = leftToRight ? 1 : -1;
	      int startIdx = -1, endIdx = -1;
	      if (leftSide) {
		startIdx = leftToRight ?  1            :  headIdx - 1;
		endIdx   = leftToRight ?  headIdx - 1  :  1;
	      }
	      else {
		startIdx = leftToRight ? headIdx + 1      :  treeListLen - 1;
		endIdx   = leftToRight ? treeListLen - 1  :  headIdx + 1;
	      }
	      // start looking one after (or before) the head index for
	      // first occurrence of a symbol in the search set, which is
	      // comprised of the symbols at indices 2..searchInstructionLen
	      // in the list searchInstruction
	      SEARCH:
	      for (int i = startIdx;
		   leftToRight ? i <= endIdx : i >= endIdx; i += increment) {
		Symbol currChild = treeList.getChildLabel(i);
		Symbol noAugChild = treebank.stripAugmentation(currChild);
		for (int j = searchSetStartIdx; j < searchInstructionLen; j++) {
		  Symbol searchSym = searchInstruction.symbolAt(j);
		  //if (noAugChild == searchSym) {
		  if (currChild == searchSym) {
		    if (negativeSearch)
		      continue SEARCH;
		    else {
		      argIdx = i;
		      child = currChild;
		      foundArg = true;
		      break SEARCH;
		    }
		  }
		}
		// if no match for any nt in search set and this is
		// negative search, we've found what we're looking for
		if (negativeSearch) {
		  argIdx = i;
		  child = currChild;
		  foundArg = true;
		  break SEARCH;
		}
	      }
	    }
	  }

	  if (foundArg) {
	    Nonterminal parsedChild =
	      treebank.parseNonterminal(child, nonterminal);
            
            System.out.println("[JAVARGAS] Training.identifyArguments Antes de addArgAugmentation  child: " + child.toString() );
            
            addArgAugmentation(child, parsedChild);
            
            System.out.println("[JAVARGAS] Training.identifyArguments Despues de addArgAugmentation parsedChild: " + parsedChild.toString() );
            
	    treeList.setChildLabel(argIdx, parsedChild.toSymbol());
	  }
	}
	else {
	  // the candidate pattern list is a list of actual nonterminal labels

	  // the following line means we only find arguments in situations
	  // where the head child's label is different from the parent label
	  if (treebank.getCanonical(headChild) !=
	      treebank.getCanonical(parent)) {
	  //if (true) {
	    relabelArgChildren(treeList, headIdx, candidatePatterns);
	  }
	}
      }
    }
    
    
      System.out.println("JAVARGAS - FIN :: Training.identifyArguments tree: " + tree.toString());

    return tree;
  }
        
/**
 * Fase 0: Leyendo arboles y encontrando nucleos.
 * 
 * @param tree
 * @return 
 */
  public Sexp preProcess(Sexp tree) {
    //transformSubjectNTs(tree);
    super.preProcess(tree);    
    fixSubjectlessSentences(tree);       
    return tree;
  }

  public boolean removeWord(Symbol word, Symbol tag, int idx, SexpList sentence,
			    SexpList tags, SexpList originalTags,
			    Set prunedPretermsPosSet,
			    Map prunedPretermsPosMap) {
      
    boolean singleQuote = word.toString().equals("'");
    String prevWordStr = idx > 0 ? sentence.symbolAt(idx - 1).toString() : "";
    boolean assumedPossessive = singleQuote && prevWordStr.endsWith("s");
    // if this isn't a possessive then set part-of-speech tag to be right
    // double-quotation mark
    if (singleQuote && !assumedPossessive && originalTags != null) {
      originalTags.set(idx, new SexpList(1).add(Symbol.add("''")));
    }
       
    return prunedPretermsPosMap.containsKey(word) && !assumedPossessive;
  }


  /**
   * We override {@link
   * danbikel.parser.Training#relabelSubjectlessSentences(Sexp)} so
   * that we can make the definition of a subjectless sentence
   * slightly more restrictive: a subjectless sentence not only must
   * have a null-element child that is marked with the subject
   * augmentation, but also its head must be a <tt>VP</tt> (this is
   * Mike Collins' definition of a subjectless sentence).
   */
  public Sexp relabelSubjectlessSentences(Sexp tree) {
    if (tree.isSymbol())
      return tree;
    if (treebank.isPreterminal(tree))
      return tree;
    if (tree.isList()) {
      SexpList treeList = tree.list();
      int treeListLen = treeList.length();

      // first, make recursive call
      for (int i = 1; i < treeList.length(); i++)
	relabelSubjectlessSentences(treeList.get(i));

      Symbol parent = treeList.symbolAt(0);
      int headIdx = headFinder.findHead(treeList);
      if (headIdx == 0)
	System.err.println(className +
			   ": error: couldn't find head for tree: " + tree);
      Symbol headChildLabel = treeList.getChildLabel(headIdx);
      Symbol sg = treebank.subjectlessSentenceLabel();
      /*
      if (treebank.isSentence(parent) &&
	  isCoordinatedPhrase(treeList, headIdx) &&
	  treebank.stripAugmentation(headChildLabel) == sg) {
	// this is a subjectless sentence, because it is an S that is
	// a coordinated phrase and whose head is a subjectless
	// sentence
	Nonterminal parsedParent = treebank.parseNonterminal(parent,
							     nonterminal);
	parsedParent.base = treebank.subjectlessSentenceLabel();
	treeList.set(0, parsedParent.toSymbol());
      }
      else */if (treebank.isSentence(parent) &&
	  treebank.getCanonical(headChildLabel) == VP) {
	for (int i = 1; i < treeListLen; i++) {
	  SexpList child = treeList.listAt(i);
	  Symbol childLabel = treeList.getChildLabel(i);
	  Nonterminal parsedChild =
	    treebank.parseNonterminal(childLabel, nonterminal);
	  Symbol subjectAugmentation = treebank.subjectAugmentation();

	  if (parsedChild.augmentations.contains(subjectAugmentation) &&
	      unaryProductionsToNull(child.get(1))) {
	    // we've got ourselves a subjectless sentence!
	    Nonterminal parsedParent = treebank.parseNonterminal(parent,
								 nonterminal);
	    parsedParent.base = treebank.subjectlessSentenceLabel();
	    treeList.set(0, parsedParent.toSymbol());
	    break;
	  }
	}
      }
    }
    
    return tree;
  }

  protected boolean isTypeOfSentence(Symbol label) {
    return label.toString().charAt(0) == 'S';
  }

  /**
   * De-transforms sentence labels changed by {@link
   * #relabelSubjectlessSentences(Sexp)} when the subjectless sentence node has
   * children prior to its head child that are arguments.
   */
  public Sexp fixSubjectlessSentences(Sexp tree) {      
    if (treebank.isPreterminal(tree))
      return tree;
    
    if (tree.isList()) {
      SexpList treeList = tree.list();
      int treeListLen = treeList.length();
      Symbol parent = treeList.symbolAt(0);
      Symbol subjectlessSentenceLabel = treebank.subjectlessSentenceLabel();
      
      if (treebank.stripAugmentation(parent) == subjectlessSentenceLabel) {
	// first, find head
          
	int headIdx = headFinder.findHead(treeList);
	// if there are arguments prior to head, this must be changed back
	// to a regular sentence label
	for (int i = headIdx - 1; i >= 1; i--) {
	  SexpList child = treeList.listAt(i);
	  Symbol childLabel = treeList.getChildLabel(i);
          
	  if (isArgumentFast(childLabel)) {
	    treeList.set(0, treebank.sentenceLabel());
	    break;
	  }
	}
      }
      
      for (int i = 1; i < treeList.length(); i++)
	fixSubjectlessSentences(treeList.get(i));
    }
    return tree;
  }

  /**
   * Attempts to un-do the transformation performed by {@link
   * #repairBaseNPs(Sexp)}, in which sentential nodes that occur to the right of
   * the head child of a base NP are moved to become immediate right siblings of
   * the base NP; accordingly, this method moves all such sentential nodes that
   * occur immediately to the right of a base NP to be the rightmost child under
   * that base NP.
   *
   * @param tree the tree for which NPs that were transformed by {@link
   *             #repairBaseNPs(Sexp)} are to be de-transformed
   * @return the specified tree, with certain NPs de-transformed
   */
  protected Sexp unrepairBaseNPs(Sexp tree) {
    if (tree.isSymbol())
      return tree;
    if (treebank.isPreterminal(tree))
      return tree;
    if (tree.isList()) {
      SexpList treeList = tree.list();
      // if we find a base NP followed by any type of S, unhook the S
      // from its parent and put as new final child of base NP subtree
      boolean thereAreAtLeastTwoChildren = treeList.length() > 2;
      if (thereAreAtLeastTwoChildren) {
	for (int i = 1; i < treeList.length() - 1; i++) {
	  Symbol currLabel = treeList.getChildLabel(i);
	  Symbol nextLabel = treeList.getChildLabel(i + 1);
	  if (currLabel == baseNP && isTypeOfSentence(nextLabel)) {
	    SexpList npbTree = treeList.listAt(i);
	    Sexp sentence = treeList.remove(i + 1); // unhook S from its parent
	    npbTree.add(sentence);
	    break;
	  }
	}
      }

      int treeListLen = treeList.length();
      for (int i = 1; i < treeListLen; i++)
	unrepairBaseNPs(treeList.get(i));
    }
    return tree;
  }

  public void postProcess(Sexp tree) {
    super.postProcess(tree);
      
    System.out.println("EOF");
  }


  /** Test driver for this class. */
  public static void main(String[] args) {
            
    String filename = null;
    boolean raisePunc = false, idArgs = true/*false*/, subjectlessS = false;
    boolean stripAug = false, addBaseNPs = false;

    for (int i = 0; i < args.length; i++) {
      if (args[i].charAt(0) == '-') {
	if (args[i].equals("-r"))
	  raisePunc = true;
	else if (args[i].equals("-i"))
	  idArgs = true;
	else if (args[i].equals("-s"))
	  subjectlessS = true;
	else if (args[i].equals("-a"))
	  stripAug = true;
	else if (args[i].equals("-n"))
	  addBaseNPs = true;
      }
      else
	filename = args[i];
    }

    if (filename == null) {
      System.err.println("usage: [-risan] <filename>\n" +
			 "where\n\t" +
			 "-r: raise punctuation\n\t" +
			 "-i: identify arguments\n\t" +
			 "-s: relabel subjectless sentences\n\t" +
			 "-a: strip augmentations\n\t" +
			 "-n: add/relabel base NPs");
      System.exit(1);
    }

    Training training = (Training)Language.training();
    training.printMetadata();

    try {
      SexpTokenizer tok = new SexpTokenizer(filename, Language.encoding(),
					    Constants.defaultFileBufsize);
      Sexp curr = null;
      while ((curr = Sexp.read(tok)) != null) {
	if (raisePunc) { System.out.println("[Training.main] => raisePunctuation");
	  System.out.println(training.raisePunctuation(curr));
        } if (idArgs) { System.out.println("[Training.main] => identifyArguments");
	  System.out.println(training.identifyArguments(curr));
        } if (subjectlessS) { System.out.println("[Training.main] => relabelSubjectlessSentences");
	  System.out.println(training.relabelSubjectlessSentences(curr));
        } if (stripAug) { System.out.println("[Training.main] => stripAugmentations");
	  System.out.println(training.stripAugmentations(curr));
        } if (addBaseNPs) { System.out.println("[Training.main] => addBaseNPs");
	  System.out.println(training.addBaseNPs(curr));
        }
      }
      System.out.println("\n\n");
    }
    catch (UnsupportedEncodingException uee) {
      System.err.println(uee);
    }
    catch (FileNotFoundException fnfe) {
      System.err.println(fnfe);
    }
    catch (IOException ioe) {
      System.err.println(ioe);
    }      
  } 
  
}
