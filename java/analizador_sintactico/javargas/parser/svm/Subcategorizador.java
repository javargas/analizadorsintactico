
package javargas.parser.svm;

import danbikel.lisp.SexpList;
import danbikel.lisp.Symbol;
import danbikel.parser.CKYItem;
import danbikel.parser.Subcat;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Esta clase es la encargada de redefinir el marco de subcategorizacion de un 
 * verbo (núcleo de una oración)
 *
 * @author javargas
 */
public class Subcategorizador {
    
    /* Mapeo de argumentos a posibles nodos cabeza.  <file: etiquetes_func_arg_PT.pdf> */
    private HashMap<String, String[]> mapArgNodos = new HashMap(); 
    
    /* Mapeo de verbos a lemas */
    private HashMap<String, String> mapLemas = new HashMap();
    
    /* Método constructor por defecto. */
    public Subcategorizador()
    {
        llenarMapArgNodos();
        
        cargarLemasVerbos();
    }
    
    /**
     * Se encarga de poblar la estructura de mapeo de argumentos a nodos cabeza.
     */
    private void llenarMapArgNodos()
    {
        mapArgNodos.put("suj", new String[]{"S", "sn", "sp", "relatiu"});
        mapArgNodos.put("cd", new String[]{"S", "sa", "sadv", "sn", "sp", "relatiu"});
        mapArgNodos.put("creg", new String[]{"S", "sadv", "sp", "relatiu"});
        mapArgNodos.put("ci", new String[]{"sp", "sn", "relatiu"});
        mapArgNodos.put("cc", new String[]{"S", "sa", "sadv", "sn", "sp", "relatiu"});
        mapArgNodos.put("cag", new String[]{"sp"});
        mapArgNodos.put("cpred", new String[]{"S", "sa", "sadv", "sn", "sp"});
        mapArgNodos.put("atr", new String[]{"S", "sa", "sadv", "sn", "sp", "relatiu"});
      //  mapArgNodos.put("cag", new String[]{"sp"});
    }
    
    /**
     * Obtiene la lista de posibles nodos cabeza para el argumento dado
     * 
     * @param argumento 
     * @return  Lista de posibles nodos cabeza.
     */
    public String[] getPosiblesHeadNodes(String argumento)
    {
        return (String[])mapArgNodos.get(argumento);
    }
    
    private void cargarLemasVerbos()
    {    
        return;
        /*
        String strArchivoLemas = "./archivoLemas";
            
        try{
            FileInputStream fstream = new FileInputStream(strArchivoLemas);
            try (DataInputStream entrada = new DataInputStream(fstream)) {
                BufferedReader buffer = new BufferedReader(new InputStreamReader(entrada));
                String strLinea;
                
                while ((strLinea = buffer.readLine()) != null && !strLinea.equals("")) {
                    String[] datosLinea = strLinea.split(":");
                    mapLemas.put(datosLinea[0], datosLinea[1]);
                }
            }
        } catch (IOException e){ 
            System.err.println("Ocurrio un error: " + e.getMessage());
        }*/
    }
    
    /**
     * Reasigna un marco de subcategorización para el item dado.
     * El marco de subcategorización se construye leyendo los lemas,
     * de las palabras de la frase, y buscando en Ancora-verb los argumentos
     * requeridos para los verbos.
     * 
     * @param item
     * @param subcategoria 
     */
    public void setSubcat(CKYItem item, Subcat subcategoria)
    {                   
        /* Consultar si el item es un terminal */        
        if (item.isPreterminal()){
            
            SexpList treeList = item.toSexp().list();
            String strVerbo = treeList.symbolAt(1).toString();
            System.out.println("Obteniendo lema del mapeo, strVerbo: " + strVerbo );
            //return;
            
            
            /* Obtener el lema del verbo para buscar los argumentos requeridos. */
            String strLemaVerbo = mapLemas.get(strVerbo);
            
            System.out.println("Obteniendo lema del mapeo, strVerbo: " + strVerbo + ", streLemaVerbo: " + strLemaVerbo);
            
            Symbol tag = (Symbol)item.label();
            String strNameFile = "./corpus/ancora-verb-es/" + strLemaVerbo + ".lex.xml";

            File file = new File(strNameFile);

            if (file.exists()) { 
                try{
                    DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                    DocumentBuilder db = dbf.newDocumentBuilder();
                    Document doc = db.parse(file);
                    doc.getDocumentElement().normalize();

                    //System.out.println("Root element " + doc.getDocumentElement().getNodeName());
                    
                    NodeList nodeLstFrames = doc.getElementsByTagName("frame");
                    int numNodosHijos = 100000;

                    /* Recorrer los nodos y seleccionar el que tenga menor numero de argumentos */
                    Node nodoFrameSeleccionado = null;
                    for (int i = 0; i < nodeLstFrames.getLength(); i++) {
                        Node fstNodeFrame = nodeLstFrames.item(i);
                    
                        NodeList nodeLst = fstNodeFrame.getChildNodes();
                        int cantidadNodos = nodeLst.getLength();
                        
                        if (cantidadNodos < numNodosHijos) {
                            nodoFrameSeleccionado = fstNodeFrame;
                        }
                        
                    }

                    /* Se toman los argumentos qque son los nodos hijos del frame */
                    if (nodoFrameSeleccionado != null) {
                        NodeList nodeLst = nodoFrameSeleccionado.getChildNodes();

                        /* Ciclo para recorrer los argumentos del marco seleccionado */
                        for (int s = 0; s < nodeLst.getLength(); s++) {
                            Node fstNode = nodeLst.item(s);

                            if (fstNode.getNodeType() == Node.ELEMENT_NODE) {
                                Element fstElmnt = (Element) fstNode;
                                String argumento =  fstElmnt.getAttribute("function");

                                /* Obtener los posibles nodos cabeza del argumento */
                                String[] listaNodos = getPosiblesHeadNodes(argumento);
                                
                                for (int j = 0; j < listaNodos.length; j++) {
                                     Symbol simbolo = Symbol.get(listaNodos[j]);
                                     System.out.println("Subcategoria antes: " + subcategoria.toString() + " adicionando simbolo: " + simbolo);
                                     subcategoria.add(simbolo);
                                     System.out.println("Subcategoria despues: " + subcategoria.toString());
                                }
                                
                                System.out.println("Argumento: " + argumento);                  
                            }
                        }
                    }
                } catch (Exception e){ 
                    System.err.println("Ocurrio un error: " + e.getMessage());
                }     
            } /* */
        }                    
    }
}
