
package javargas.parser.svm;

import danbikel.lisp.Sexp;
import danbikel.lisp.SexpList;
import danbikel.lisp.Symbol;
import danbikel.parser.HeadFinder;
import danbikel.parser.Language;
import danbikel.parser.Treebank;
import java.util.HashMap;

/**
 * Esta clase se encarga de obtener el vector de caracteristicas para libsvm
 * a partir de las etiquetas asignadas a cada palabra por freeling.
 * 
 * @author javargas
 */
public class ExtractorCaracteristicas {
    private final HashMap<String, Integer>[][] arrEtiquetas = new HashMap[12][];
    private final HashMap<String, Integer> arrIndices = new HashMap();
    
    
    public ExtractorCaracteristicas()
    {
        llenarTabla();
    }
    
    
  public String obtenerCaracteristicas(Sexp tree, Sexp objNodoPadre)
  {
    System.out.println("[JAVARGAS] ExtractorCaracteristicas.obtenerCaracteristicas ");
    System.out.println("[JAVARGAS] *** Obteniendo las caracteristicas del arbol: " + tree.toString() + " *** ");
    String vectorCaracteristicas = "";
    SexpList treeList = tree.list();
    int headIdx = 0;
    Treebank treebank = Language.treebank();
    
    // Posicion del nucleo
    int numCaracteristica = 1;
    if (!treebank.isPreterminal(tree))
    {
      HeadFinder headFinder = Language.headFinder();
      headIdx = headFinder.findHead(tree);
    }
    //   System.out.println("Cabeza encontrada: " + headIdx + " numCaracteristica: " + numCaracteristica);
    int[] arrBinaryPositionHead = getBinaryCode(headIdx, 4);
    for (int i = 0; i < arrBinaryPositionHead.length; i++) {
      vectorCaracteristicas = vectorCaracteristicas + numCaracteristica++ + ":" + arrBinaryPositionHead[i] + " ";
        //System.out.println("**>" + vectorCaracteristicas);
    }
    
    // Codificacion de la etiqueta del nodo raiz
    Symbol nodoRaiz = treeList.first().symbol();
    System.out.println(" +++ Nodo raiz " + nodoRaiz.toString());
    
    int[] arrBinaryTerminalCode = getBinaryCode(mappingTerminarCode(nodoRaiz.toString()), 3);
    for (int i = 0; i < arrBinaryTerminalCode.length; i++) {
      vectorCaracteristicas = vectorCaracteristicas + numCaracteristica++ + ":" + arrBinaryTerminalCode[i] + " ";
       // System.out.println("**>" + vectorCaracteristicas);
    }
    System.out.println("\n=====================================================");
        
    // Numero de hijos del nodo padre.
    System.out.println("\nArbol Actual: " + tree.toString());
    if (objNodoPadre != null) {
      System.out.println("\nArbol Padre: " + objNodoPadre.toString());
    } else {
      System.out.println("\nArbol Padre: null ");
    }
    System.out.println("=====================================================\n");
    
    // Longitud del arbol
    int numAridad = treeList.size();
    System.out.println("=====================================================\n");
    
    int[] arrBinaryAridad = getBinaryCode(numAridad, 4);
    for (int i = 0; i < arrBinaryAridad.length; i++) {
      vectorCaracteristicas = vectorCaracteristicas + numCaracteristica++ + ":" + arrBinaryAridad[i] + " ";
       // System.out.println("**>" + vectorCaracteristicas);
    }
    System.out.println("Caracteristica # 3 (Aridad del nodo padre): " + numAridad);
    System.out.println("=====================================================\n");
    

    String strArbol = tree.toString();
    int numHojas = 0;int posCerrar = 0;int posAbrir = 0;
    posAbrir = strArbol.indexOf("(", posCerrar);
    while ((posAbrir != -1) && (posCerrar != -1))
    {
      posCerrar = strArbol.indexOf(")", posAbrir + 1);
      posAbrir = strArbol.indexOf("(", posCerrar + 1);
      if (posCerrar != -1) {
        numHojas++;
      }
    }
    
    // Distancia: numero de palabras entre el nodo del constituyente modificador y el verbo.
    System.out.println("Caracteristica # 4 (Numero de hojas): " + numHojas);
    
    int[] arrBinaryAnchura = getBinaryCode(numHojas, 7);
    for (int i = 0; i < arrBinaryAnchura.length; i++) {
      vectorCaracteristicas = vectorCaracteristicas + numCaracteristica++ + ":" + arrBinaryAnchura[i] + " ";
       // System.out.println("**>" + vectorCaracteristicas);
    }
    int[] arrBinaryLongitud = getBinaryCode(treeList.length(), 7);
    for (int i = 0; i < arrBinaryLongitud.length; i++) {
      vectorCaracteristicas = vectorCaracteristicas + numCaracteristica++ + ":" + arrBinaryLongitud[i] + " ";
       // System.out.println("**>" + vectorCaracteristicas);
    }
    System.out.println("[Salida] ExtractorCaracteristicas.obtenerCaracteristicas vectorCaracteristicas: " + vectorCaracteristicas);
    return vectorCaracteristicas;
  }
  
  private int[] getBinaryCode(int number, int size)
  {
    int[] arrBinaryCode = new int[size];
    int pos = 0;
    while ((number > 1) && (pos < size))
    {
      arrBinaryCode[(pos++)] = (number % 2);
      number /= 2;
    }
    if (pos < size) {
      arrBinaryCode[(pos++)] = number;
    }
    while (pos < size) {
      arrBinaryCode[(pos++)] = 0;
    }
    return arrBinaryCode;
  }
  
  private int mappingTerminarCode(String tag)
  {
    int code = 6;
    if ((tag.equals("snp")) || (tag.equals("sno")) || (tag.equals("sn.e")) || (tag.equals("sn")) || (tag.equals("relatiu")) || (tag.equals("sn.x"))) {
      code = 1;
    }
    if (tag.equals("sa")) {
      code = 2;
    }
    if ((tag.equals("S.F.C")) || (tag.equals("S.NF.C"))) {
      code = 3;
    }
    if (tag.equals("sp")) {
      code = 4;
    }
    if (tag.equals("neg")) {
      code = 5;
    }
    return code;
  }
  
    public String obtenerCaracteristicas(String etiqueta)
    {
        String vectorCaracteristicas = "";
        int posMainTag = 0;
        String tag = "";
        
        for (int i = 0; i < etiqueta.length(); i++) {
            tag = etiqueta.substring(i, i+1);
            //System.out.println("Tag: " + tag);
            
            if (i == 0) {
                posMainTag = ((Integer)arrIndices.get(tag)).intValue();
            }
            
            Integer posTag = (Integer)arrEtiquetas[posMainTag][i].get(tag);            
            vectorCaracteristicas += /*"("+tag+")" + */posTag.toString()+":1 ";            
            
            if (tag.equals("F")) return vectorCaracteristicas;
        }
        
        return vectorCaracteristicas;
    }
    
    private void llenarTabla()
    {
        int posicion = 1;                
        
        arrIndices.put("A", new Integer(0));        
        arrIndices.put("R", new Integer(1));
        arrIndices.put("D", new Integer(2));
        arrIndices.put("N", new Integer(3));
        arrIndices.put("V", new Integer(4));
        arrIndices.put("P", new Integer(5));
        arrIndices.put("C", new Integer(6));
        arrIndices.put("I", new Integer(7));
        arrIndices.put("S", new Integer(8));
        arrIndices.put("F", new Integer(9));
        arrIndices.put("Z", new Integer(10));
        arrIndices.put("W", new Integer(11));
        
        /* Categoria */
        arrEtiquetas[0] = new HashMap[6];
        arrEtiquetas[0][0] = new HashMap();
        arrEtiquetas[0][0].put("A", new Integer(posicion++)); /* Adjetivo */ 
        
        /* Tipo */
        arrEtiquetas[0][1] = new HashMap();
        arrEtiquetas[0][1].put("Q", new Integer(posicion++)); /* Calificativo */
        arrEtiquetas[0][1].put("O", new Integer(posicion++)); /* Ordinal */
        
        /* Grado */
        arrEtiquetas[0][2] = new HashMap();
        arrEtiquetas[0][2].put("A", new Integer(posicion++)); /* Aumentativo */
        arrEtiquetas[0][2].put("D", new Integer(posicion++)); /* Diminutivo */ 
        arrEtiquetas[0][2].put("C", new Integer(posicion++)); /* Comparativo */
        arrEtiquetas[0][2].put("S", new Integer(posicion++)); /* Superlativo */  
        arrEtiquetas[0][2].put("0", new Integer(posicion++)); /* demas */  
        
        /* Género */ 
        arrEtiquetas[0][3] = new HashMap();
        arrEtiquetas[0][3].put("M", new Integer(posicion++)); /* Masculino */  
        arrEtiquetas[0][3].put("F", new Integer(posicion++)); /* Femenino */  
        arrEtiquetas[0][3].put("C", new Integer(posicion++)); /* Común */  
        
        /* Número */
        arrEtiquetas[0][4] = new HashMap();
        arrEtiquetas[0][4].put("S", new Integer(posicion++)); /* Singular */  
        arrEtiquetas[0][4].put("P", new Integer(posicion++)); /* Plural */  
        arrEtiquetas[0][4].put("N", new Integer(posicion++)); /* Invariable */  
        
        /* Función */
        arrEtiquetas[0][5] = new HashMap();
        arrEtiquetas[0][5].put("0", new Integer(posicion++)); /* - */  
        arrEtiquetas[0][5].put("P", new Integer(posicion++)); /* Participio */  
        
        /* Categoria */
        arrEtiquetas[1] = new HashMap[2];
        arrEtiquetas[1][0] = new HashMap();
        arrEtiquetas[1][0].put("R", new Integer(2)); /* Adverbio */
        
        /* Tipo */
        arrEtiquetas[1][1] = new HashMap();
        arrEtiquetas[1][1].put("G", new Integer(posicion++)); /* Genero */
        arrEtiquetas[1][1].put("N", new Integer(posicion++)); /* Negativo */
        
        /* Categoria */
        arrEtiquetas[2] = new HashMap[7];
        arrEtiquetas[2][0] = new HashMap();
        arrEtiquetas[2][0].put("D", new Integer(3)); /* Determinante */
        
        /* Tipo */
        arrEtiquetas[2][1] = new HashMap();
        arrEtiquetas[2][1].put("D", new Integer(posicion++)); /* Demostrativo */
        arrEtiquetas[2][1].put("P", new Integer(posicion++)); /* Posesivo */
        arrEtiquetas[2][1].put("T", new Integer(posicion++)); /* Interrogativo */
        arrEtiquetas[2][1].put("E", new Integer(posicion++)); /* Exclamativo */
        arrEtiquetas[2][1].put("I", new Integer(posicion++)); /* Indefinido */
        arrEtiquetas[2][1].put("A", new Integer(posicion++)); /* Articulo */
        
        /* Persona */
        arrEtiquetas[2][2] = new HashMap();
        arrEtiquetas[2][2].put("1", new Integer(posicion++)); /* Primera */
        arrEtiquetas[2][2].put("2", new Integer(posicion++)); /* Segunda */ 
        arrEtiquetas[2][2].put("3", new Integer(posicion++)); /* Tercera */
        arrEtiquetas[2][2].put("0", new Integer(posicion++)); /* Valor por defecto */
        
        /* Género */ 
        arrEtiquetas[2][3] = new HashMap();
        arrEtiquetas[2][3].put("M", new Integer(posicion++)); /* Masculino */  
        arrEtiquetas[2][3].put("F", new Integer(posicion++)); /* Femenino */  
        arrEtiquetas[2][3].put("C", new Integer(posicion++)); /* Común */  
        arrEtiquetas[2][3].put("N", new Integer(posicion++)); /* Neutro */  
        
        /* Numero */
        arrEtiquetas[2][4] = new HashMap();
        arrEtiquetas[2][4].put("S", new Integer(posicion++)); /* Singular */  
        arrEtiquetas[2][4].put("P", new Integer(posicion++)); /* Plural */  
        arrEtiquetas[2][4].put("N", new Integer(posicion++)); /* Invariable */  
        
        /* Poseedor */
        arrEtiquetas[2][5] = new HashMap();
        arrEtiquetas[2][5].put("S", new Integer(posicion++)); /* Singular */  
        arrEtiquetas[2][5].put("P", new Integer(posicion++)); /* Plural */  
        arrEtiquetas[2][5].put("0", new Integer(posicion++)); /* para terceros */  
           
        /* Categoria */
        arrEtiquetas[3] = new HashMap[7];
        arrEtiquetas[3][0] = new HashMap();
        arrEtiquetas[3][0].put("N", new Integer(posicion++)); /* Nombre */
        
        /* Tipo */        
        arrEtiquetas[3][1] = new HashMap();
        arrEtiquetas[3][1].put("C", new Integer(posicion++)); /* Común */
        arrEtiquetas[3][1].put("P", new Integer(posicion++)); /* Propio */
        
        /* Género */ 
        arrEtiquetas[3][2] = new HashMap();
        arrEtiquetas[3][2].put("M", new Integer(posicion++)); /* Masculino */  
        arrEtiquetas[3][2].put("F", new Integer(posicion++)); /* Femenino */  
        arrEtiquetas[3][2].put("C", new Integer(posicion++)); /* Común */    
        arrEtiquetas[3][2].put("0", new Integer(posicion++)); /* -- */  
        
        /* Número */
        arrEtiquetas[3][3] = new HashMap();
        arrEtiquetas[3][3].put("S", new Integer(posicion++)); /* Singular */  
        arrEtiquetas[3][3].put("P", new Integer(posicion++)); /* Plural */  
        arrEtiquetas[3][3].put("N", new Integer(posicion++)); /* Invariable */  
        arrEtiquetas[3][3].put("0", new Integer(posicion++)); /* -- */  
        
        /* Clasificación semántica */
        arrEtiquetas[3][4] = new HashMap();
        arrEtiquetas[3][4].put("S", new Integer(posicion++)); /* Persona */  
        arrEtiquetas[3][4].put("G", new Integer(posicion++)); /* Lugar */  
        arrEtiquetas[3][4].put("O", new Integer(posicion++)); /* Organizacion */  
        arrEtiquetas[3][4].put("V", new Integer(posicion++)); /* Otros */  
        arrEtiquetas[3][4].put("0", new Integer(posicion++)); /* -- */  
        
        
        arrEtiquetas[3][5] = new HashMap();
        arrEtiquetas[3][5].put("P", new Integer(posicion++)); /* -- */  
        arrEtiquetas[3][5].put("0", new Integer(posicion++)); /* -- */  
        
        /* Grado */
        arrEtiquetas[3][6] = new HashMap();
        arrEtiquetas[3][6].put("A", new Integer(posicion++)); /* Aumentativo */  
        arrEtiquetas[3][6].put("D", new Integer(posicion++)); /* Dimnutivo */  
        arrEtiquetas[3][6].put("0", new Integer(posicion++)); /* -- */  
        arrEtiquetas[3][6].put("L", new Integer(posicion++)); /* -- */  
        
        /* Categoria */
        arrEtiquetas[4] = new HashMap[7];
        arrEtiquetas[4][0] = new HashMap();
        arrEtiquetas[4][0].put("V", new Integer(posicion++)); /* Verbo */
        
        /* Tipo */
        arrEtiquetas[4][1] = new HashMap();
        arrEtiquetas[4][1].put("M", new Integer(posicion++)); /* Principal */
        arrEtiquetas[4][1].put("A", new Integer(posicion++)); /* Auxiliar */
        arrEtiquetas[4][1].put("S", new Integer(posicion++)); /* Semiauxiliar */
        
        /* Modo */
        arrEtiquetas[4][2] = new HashMap();
        arrEtiquetas[4][2].put("I", new Integer(posicion++)); /* Indicativo */
        arrEtiquetas[4][2].put("S", new Integer(posicion++)); /* Subjuntivo */
        arrEtiquetas[4][2].put("M", new Integer(posicion++)); /* Imperativo */
        arrEtiquetas[4][2].put("N", new Integer(posicion++)); /* Infinitivo */
        arrEtiquetas[4][2].put("G", new Integer(posicion++)); /* Gerundio */
        arrEtiquetas[4][2].put("P", new Integer(posicion++)); /* Participio */
        
        /* Tiempo */
        arrEtiquetas[4][3] = new HashMap();
        arrEtiquetas[4][3].put("P", new Integer(posicion++)); /* Presente */
        arrEtiquetas[4][3].put("I", new Integer(posicion++)); /* Imperfecto */
        arrEtiquetas[4][3].put("F", new Integer(posicion++)); /* Futuro */
        arrEtiquetas[4][3].put("S", new Integer(posicion++)); /* Pasado */
        arrEtiquetas[4][3].put("C", new Integer(posicion++)); /* Condicional */
        arrEtiquetas[4][3].put("O", new Integer(posicion++)); /* - */
        
        /* Persona */
        arrEtiquetas[4][4] = new HashMap();
        arrEtiquetas[4][4].put("1", new Integer(posicion++)); /* Primera */
        arrEtiquetas[4][4].put("2", new Integer(posicion++)); /* Segunda */ 
        arrEtiquetas[4][4].put("3", new Integer(posicion++)); /* Tercera */
        
        /* Número */
        arrEtiquetas[4][5] = new HashMap();
        arrEtiquetas[4][5].put("S", new Integer(posicion++)); /* Singular */  
        arrEtiquetas[4][5].put("P", new Integer(posicion++)); /* Plural */  
        
        /* Género */ 
        arrEtiquetas[4][6] = new HashMap();
        arrEtiquetas[4][6].put("M", new Integer(posicion++)); /* Masculino */  
        arrEtiquetas[4][6].put("F", new Integer(posicion++)); /* Femenino */ 
        arrEtiquetas[4][6].put("0", new Integer(posicion++)); /* -- */  
                
        /* Categoria */
        arrEtiquetas[5] = new HashMap[8];
        arrEtiquetas[5][0] = new HashMap();
        arrEtiquetas[5][0].put("P", new Integer(posicion++)); /* Pronombres */
        
        /* Tipo */
        arrEtiquetas[5][1] = new HashMap();
        arrEtiquetas[5][1].put("P", new Integer(posicion++)); /* Personal */
        arrEtiquetas[5][1].put("D", new Integer(posicion++)); /* Demostrativo */
        arrEtiquetas[5][1].put("X", new Integer(posicion++)); /* Posesivo */
        arrEtiquetas[5][1].put("I", new Integer(posicion++)); /* Indefinido */
        arrEtiquetas[5][1].put("T", new Integer(posicion++)); /* Interrogativo */
        arrEtiquetas[5][1].put("R", new Integer(posicion++)); /* Relativo */
        arrEtiquetas[5][1].put("E", new Integer(posicion++)); /* Exclamativo */
        
        /* Persona */
        arrEtiquetas[5][2] = new HashMap();
        arrEtiquetas[5][2].put("1", new Integer(posicion++)); /* Primera */
        arrEtiquetas[5][2].put("2", new Integer(posicion++)); /* Segunda */ 
        arrEtiquetas[5][2].put("3", new Integer(posicion++)); /* Tercera */
        
        /* Género */ 
        arrEtiquetas[5][3] = new HashMap();
        arrEtiquetas[5][3].put("M", new Integer(posicion++)); /* Masculino */  
        arrEtiquetas[5][3].put("F", new Integer(posicion++)); /* Femenino */ 
        arrEtiquetas[5][3].put("C", new Integer(posicion++)); /* Común */  
        arrEtiquetas[5][3].put("N", new Integer(posicion++)); /* Neutro */  
        
        /* Número */
        arrEtiquetas[5][4] = new HashMap();
        arrEtiquetas[5][4].put("S", new Integer(posicion++)); /* Singular */  
        arrEtiquetas[5][4].put("P", new Integer(posicion++)); /* Plural */  
        arrEtiquetas[5][4].put("N", new Integer(posicion++)); /* Impersonal/Invariable */  
        
        /* Caso */
        arrEtiquetas[5][5] = new HashMap();
        arrEtiquetas[5][5].put("N", new Integer(posicion++)); /* Nominativo */  
        arrEtiquetas[5][5].put("A", new Integer(posicion++)); /* Acusativo */  
        arrEtiquetas[5][5].put("D", new Integer(posicion++)); /* Dativo */  
        arrEtiquetas[5][5].put("O", new Integer(posicion++)); /* Oblicuo */  
        
        /* Poseedor */
        arrEtiquetas[5][6] = new HashMap();
        arrEtiquetas[5][6].put("S", new Integer(posicion++)); /* Singular */  
        arrEtiquetas[5][6].put("P", new Integer(posicion++)); /* Plural */ 
        
        /* Politeness */ 
        arrEtiquetas[5][7] = new HashMap();
        arrEtiquetas[5][7].put("P", new Integer(posicion++)); /* Polite */ 
       
        /* Categoria */
        arrEtiquetas[6] = new HashMap[2];
        arrEtiquetas[6][0] = new HashMap();
        arrEtiquetas[6][0].put("C", new Integer(posicion++)); /* Conjunciones */
        
        /* Tipo */
        arrEtiquetas[6][1] = new HashMap();
        arrEtiquetas[6][1].put("C", new Integer(posicion++)); /* Coordinada */
        arrEtiquetas[6][1].put("S", new Integer(posicion++)); /* Subordinada */
        
        /* Categoria */
        arrEtiquetas[7] = new HashMap[1];
        arrEtiquetas[7][0] = new HashMap();
        arrEtiquetas[7][0].put("I", new Integer(posicion++)); /* Interjeciones */
                
        /* Categoria */
        arrEtiquetas[8] = new HashMap[5];   
        arrEtiquetas[8][0] = new HashMap();     
        arrEtiquetas[8][0].put("S", new Integer(posicion++)); /* Preposiciones */
        
        /* Tipo */
        arrEtiquetas[8][1] = new HashMap();
        arrEtiquetas[8][1].put("P", new Integer(posicion++)); /* Preposición */
                
        /* Forma */
        arrEtiquetas[8][2] = new HashMap();
        arrEtiquetas[8][2].put("S", new Integer(posicion++)); /* Simple */
        arrEtiquetas[8][2].put("C", new Integer(posicion++)); /* Contraida */
        
        /* Genero */
        arrEtiquetas[8][3] = new HashMap();
        arrEtiquetas[8][3].put("M", new Integer(posicion++)); /* Masculino */
        arrEtiquetas[8][3].put("0", new Integer(posicion++)); /* -- */
        
        /* Numero */
        arrEtiquetas[8][4] = new HashMap();
        arrEtiquetas[8][4].put("S", new Integer(posicion++)); /* Singular */
        arrEtiquetas[8][4].put("0", new Integer(posicion++)); /* -- */
                
        /* Categoria */
        arrEtiquetas[9] = new HashMap[1];   
        arrEtiquetas[9][0] = new HashMap();
        arrEtiquetas[9][0].put("F", new Integer(posicion++)); /* Puntuación*/
                
        /* Categoria */
        arrEtiquetas[10] = new HashMap[2];   
        arrEtiquetas[10][0] = new HashMap();
        arrEtiquetas[10][0].put("Z", new Integer(posicion++)); /* Numerales */
        
        /* Tipo */
        arrEtiquetas[10][1] = new HashMap();
        arrEtiquetas[10][1].put("D", new Integer(posicion++)); /* partitivo */
        arrEtiquetas[10][1].put("M", new Integer(posicion++)); /* moneda */
        arrEtiquetas[10][1].put("P", new Integer(posicion++)); /* porcentaje */
        arrEtiquetas[10][1].put("U", new Integer(posicion++)); /* unidad */
        
        /* Categoria */
        arrEtiquetas[11] = new HashMap[1];  
        arrEtiquetas[11][0] = new HashMap();
        arrEtiquetas[11][0].put("W", new Integer(posicion++)); /* Fecha */
    }
}
