

package javargas.parser.svm;

import danbikel.lisp.Sexp;
import danbikel.lisp.SexpList;
import danbikel.lisp.Symbol;
import danbikel.parser.CKYItem;
import danbikel.util.SLNode;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.HashMap;

/**
 * Se encarga de clasificar las palabras de una sentencia en complementos y adjuntos
 * 
 * @author javargas
 */
public class Clasificador {
    
    private static boolean blnIsArgumentSVM = false;
    private final HashMap<String, Integer> clasificacion;
    private SexpList tags;
    private static int numVerbModifiersCount = 0;
    
    /* Archivo donde se dice que tipo de argumento corresponde a un tag del arbol de salida */
    private static File argumentTagsFile = new File("argumentTags.txt");

    public Clasificador() {
        clasificacion = new HashMap<String, Integer>();        
    }

    public static void setBlnIsArgumentSVM(boolean blnIsArgumentSVM) {
        Clasificador.blnIsArgumentSVM = blnIsArgumentSVM;
    }
    
    public static String clasificar (Sexp tree)
    {
        String clase = "C";               
        leerArbol(tree);        
        return clase;
    }
    
    private static void leerArbol(Sexp tree) 
    {    
        boolean blnArgumentChilds = false;
        
        if (tree.isList()) {
            SexpList treeList = tree.list();
            Symbol tagRoot = treeList.first().symbol();

            int numElements = treeList.length();

            System.out.println("TAG ROOT: " + tagRoot.toString());        
            System.out.println("LONGITUD: " + numElements);

            for (int i = 1; i < numElements; i++) {                
                System.out.println(treeList.toString());
                if (treeList.get(i).isList()) {
                    Symbol subLabel = treeList.getChildLabel(i);            
                    System.out.println("SUBLABEL: " + subLabel.toString());        

                    if ((subLabel.toString()).equals("grup.verb")) {
                        blnArgumentChilds = true;
                    }
                }                
            }
                        
            if (blnArgumentChilds) { System.out.println("clasificando: ");     
                for (int i = 1; i < numElements; i++) {
                    Symbol subLabel = treeList.getChildLabel(i);
                    
                    if (!(subLabel.toString()).equals("grup.verb")) { 
                        Sexp subTree = treeList.get(i);            
                        ejecutarSVM(subTree, tree);                    

                        int clase = obtenerClase();
                        String newLabel = "";
                        
                        if (subLabel.toString().equals("sadv") || subLabel.toString().equals("rg")) {                            
                            newLabel = subLabel.toString() + "-CC";
                            treeList.setChildLabel(i, Symbol.get(newLabel));
                        } else if (subLabel.toString().equals("sn") || subLabel.toString().equals("sno")
                                    ||  subLabel.toString().equals("S.F.C.co")) {                         
                            newLabel = subLabel.toString() + "-SUJ";
                            treeList.setChildLabel(i, Symbol.get(newLabel));
                        } else if (subLabel.toString().equals("S.F.C") || subLabel.toString().equals("S.NF.C")) {                         
                            newLabel = subLabel.toString() + "-CD";
                            treeList.setChildLabel(i, Symbol.get(newLabel));
                        } else {                          
                            
                            /*
                            switch (clase) {
                                case -1: // complemento
                                    newLabel = subLabel.toString() + "-Comp";
                                    treeList.setChildLabel(i, Symbol.get(newLabel));
                                   break; 
                                case 1: // adjunto
                                    newLabel = subLabel.toString() + "-CC";
                                    treeList.setChildLabel(i, Symbol.get(newLabel));
                                    break;
                                default:
                                    newLabel = subLabel.toString() + "-Comp";
                                    treeList.setChildLabel(i, Symbol.get(newLabel));
                            }   */
                        }
                    }
                }                
            } else { // Llamado recursivo
                for (int i = 1; i < numElements; i++) {
                    Sexp subTree = treeList.get(i);            
                    leerArbol(subTree);
                }
            }
        }
    }
        
    /**
     * Obtiene las caracteristicas del subarbol de entrada
     * y lo clasica como complemento o adjunto
     * 
     * @param tree subarbol a clasificar
     */
    public static void ejecutarSVM(Sexp tree, Sexp nodoPadre)
    {            
        ExtractorCaracteristicas extractor = new ExtractorCaracteristicas();
        
        System.out.println("En ExtractorCaracteristicas.ejecutarSVM tree:" + tree.toString());
                
        try {            
            
            /* Generar archivo de caracteristicas para predicción */  
            File file = new File("archivo_caracteristicas.t");
            
            if (file.exists()) {
                file.delete();
            }
            
            BufferedWriter output = new BufferedWriter(new FileWriter(file, true));  

            String vectorCaracteristicas = extractor.obtenerCaracteristicas(tree, nodoPadre);              
            output.append("0 "+vectorCaracteristicas+"\n");    
          
            output.close();
          
            /* Ejecucion del libSVM */
            String args[] = {"archivo_caracteristicas.t", "archivo_caracteristicas.scale.model", "salida_svm"};
            parser_svm_predict.main(args);          
        } catch (IOException e) {}
    }
   
    /**
     * Clasifica los nodos a la izquierda del item como complementos o adjuntos,
     * y modifica la subcategorizaciones de ese item
     * 
     * @param item
     */
    public boolean clasificar(CKYItem item)
    {
        boolean isArgument = false;
        System.out.println("\n[JAVARGAS] ==> Clasificador.clasificar item: " + item.toString() + "\n");

        ejecutarSVM(item);

        int clase = obtenerClase();
        System.out.println("\n[JAVARGAS] ==> Clasificador.clasificar clase: " + clase + "\n");
        SexpList treeList = item.toSexp().list();
        Symbol tag = treeList.first().symbol();

        switch (clase) {
            case -1:
                isArgument = true;
               break; 
            case 0:
                isArgument = false;
            case 1:
                isArgument = false;
                break;
        }     
        
        return isArgument;
    }
        
    public static int obtenerClase()
    {        
        int clase = 0;
        
        try{
            FileInputStream fstream = new FileInputStream("salida_svm");
            DataInputStream entrada = new DataInputStream(fstream);
            BufferedReader buffer = new BufferedReader(new InputStreamReader(entrada));
            String strLinea;

            if ((strLinea = buffer.readLine()) != null) {                
                double c = Double.parseDouble(strLinea);
                clase = (int)c;
            }
            
            entrada.close();
        } catch (IOException e){ 
            System.err.println("Ocurrio un error: " + e.getMessage());
        }
        
        return clase;
    }
    
    
    /**
     * Obtiene las caracteristicas del subarbol de entrada
     * y lo clasica como complemento o adjunto
     * 
     * @param item subarbol a clasificar
     */
    public void ejecutarSVM(CKYItem item)
    {            
        Sexp tree = item.toSexp();
        ExtractorCaracteristicas extractor = new ExtractorCaracteristicas();
        
        System.out.println("En ExtractorCaracteristicas.ejecutarSVM tree:" + tree.toString());
        
        
        
        /* Obtener el nodo padre del nodo raiz del subarbol 'tree' */        
        System.out.println("\n=====================================================");
        System.out.println("\nArbol Actual: " + tree.toString());
        SLNode nodosIzq = item.leftChildren();
        if (nodosIzq !=  null)
            System.out.println("Nodos a la izquierda: " + nodosIzq.toString());
        else 
            System.out.println("Nodos a la izquierda: null ");
        
        System.out.println("=====================================================\n");
        
        try {            
            
            /* Generar archivo de caracteristicas para predicción */  
            File file = new File("archivo_caracteristicas.t");
            
            if (file.exists()) {
                file.delete();
            }
            
            BufferedWriter output = new BufferedWriter(new FileWriter(file, true));  

            String vectorCaracteristicas = extractor.obtenerCaracteristicas(tree, null);              
            output.append("0 "+vectorCaracteristicas+"\n");    
          
            output.close();
          
            /* Ejecucion del libSVM */
            String args[] = {"archivo_caracteristicas.t", "archivo_caracteristicas.scale.model", "salida_svm"};
            parser_svm_predict.main(args);          
        } catch (IOException e) {}
    }
    
        
    
  /**
   * Este método usa el clasificador SVM para determinar 
   * un factor de probabilidad, de que el subarbol modifier 
   * sea un complemento del subarbol modificand
   * 
   * @param modificand Item núcleo, correspondiente a un verbo
   * @param modifier Item que presuntamente es un argumento del modificand
   * @return 
   */
  public static double getProbabilityFactor(CKYItem modificand, CKYItem modifier) {
    System.out.println("Clasificador.getProbabilityFactor [JAVARGAS] modificand: " + modificand.toString() + " , modifier: " + modifier.toString() ); 
    
    double numProbabilityFactor = 1.0; // Valor por defecto del factor      
    String[] arrLabelArgs = {"sadv", "sn.x", "sp", "sn", "sa", "S.F.C", "S.NF.C", "snp" };
    
    SexpList treeList = modificand.toSexp().list();

    if (treeList.size() > 2) {
        String strLabel = treeList.get(1).list().symbolAt(0).toString();

        //String prueba = treeList;
        //System.out.println(" strLabel :::  " + strLabel);

        if (strLabel.equals("grup.verb")) {
            Clasificador clasificador = new Clasificador();       
            
            SexpList treeListMod = modifier.toSexp().list();

            String strLabelMod = treeListMod.symbolAt(0).toString();
            System.out.println(" strLabel Modifier :::  " + strLabelMod);
            
            

            if (strLabelMod.equals("sadv") || strLabelMod.equals("rg")) {                           
                numProbabilityFactor = 1.01;
            } else if (strLabelMod.equals("sn") || strLabelMod.equals("sno")
                        ||  strLabelMod.equals("S.F.C.co")) {                         
                numProbabilityFactor = 1.01;
            } else if (strLabelMod.equals("S.F.C") || strLabelMod.equals("S.NF.C")) {                         
                numProbabilityFactor = 1.01;
            } else {

                if (Arrays.asList(arrLabelArgs).contains(strLabelMod) ) {               

                    clasificador.ejecutarSVM(modifier);

                    int clase = clasificador.obtenerClase();
                    System.out.println("\n[JAVARGAS] ==> Clasificador.clasificar clase: " + clase + "\n");
                    SexpList treeListModifier = modifier.toSexp().list();
                    Symbol tag = treeListModifier.first().symbol();

                    switch (clase) {
                        case 1:
                                numProbabilityFactor = 1.00;
                                break;
                        default:                         
                                numProbabilityFactor = 0.99;
                    }                
                }
                
                numVerbModifiersCount++;
            }
            
            System.out.println(numVerbModifiersCount + " : Clasificado isArgument: " + numProbabilityFactor);
        }
    }
    numProbabilityFactor = 1.0;
      
    return numProbabilityFactor;      
  }    
    
    
  /**
   * Este método usa el clasificador SVM para determinar si
   * un item modificador es argumento del item a modificar.
   * 
   * @param modificand Item núcleo, correspondiente a un verbo
   * @param modifier Item que presuntamente es un argumento del modificand
   * @return 
   */
  public static boolean isArgument(CKYItem modificand, CKYItem modifier) {
    System.out.println("Clasificador.isArgument [JAVARGAS] modificand: " + modificand.toString() + " , modifier: " + modifier.toString() ); 

    boolean blnIsArgument = false;      
    String[] arrLabelArgs = {"sadv", "sn.x", "sp", "sn", "sa", "S.F.C", "S.NF.C", "snp" };
    
    SexpList treeList = modificand.toSexp().list();

    if (treeList.size() > 2) {
        String strLabel = treeList.get(1).list().symbolAt(0).toString();

        //String prueba = treeList;
        //System.out.println(" strLabel :::  " + strLabel);

        if (strLabel.equals("grup.verb")) {
            Clasificador clasificador = new Clasificador();       
            
            SexpList treeListMod = modifier.toSexp().list();

            String strLabelMod = treeListMod.symbolAt(0).toString();
            //System.out.println(" strLabel Modifier :::  " + strLabelMod);

            if (Arrays.asList(arrLabelArgs).contains(strLabelMod) ) {
                blnIsArgument = clasificador.clasificar(modifier);            
                numVerbModifiersCount++;
            }
            
            /*
            Subcategorizador subcategorizador = new Subcategorizador();
            Subcat thisSideSubcat = (Subcat)modificand.subcat(true);            
            System.out.println(" Clasificador.isArgument Subcat antes: " + thisSideSubcat.toString());            
            subcategorizador.setSubcat(modificand, thisSideSubcat);                        
            System.out.println(" Clasificador.isArgument Subcat despues: " + thisSideSubcat.toString());
            */
            System.out.println(numVerbModifiersCount + " : Clasificado isArgument: " + (blnIsArgument?" true ":" false "));
        }
    }
      
    return blnIsArgument;      
  }
  
  
  public static boolean isArgumentFast(Symbol requirement) {      
    if (!blnIsArgumentSVM) {
        return false;
    }    
    String strLabelM = requirement.toString();
    return strLabelM.equals("sadv") || strLabelM.equals("sn") || strLabelM.equals("sp");
  }
    
    public boolean esComplemento(Symbol tag)
    {        
        Integer clase = (Integer)clasificacion.get((tag.toString()).toUpperCase());
        return true;
        //return (clase != null) ? (clase.intValue() != 0) : false;
    }
}
