<?php

/**
 * Este script se encarga de mostar el log del proceso que sigue el clasificador
 * de argumentos de verbo, para el analizador sintactico probabilistico de Bikel.
 *
 * @author John Vargas <javragas@gmail.com>
 * @date Dic 16 de 2014
 */

$cmd = "tail -n 30 salida_parser_bikel";

//echo $cmd; 
$shellOutput = shell_exec($cmd);
echo '<pre>';
print_r($shellOutput);  
echo '</pre>';
?>