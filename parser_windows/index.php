<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="John A. Vargas <javargas@gmail.com>">
    <title>Analizador Sintactico</title>
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/bootstrap-theme.min.css" rel="stylesheet">
    <link href="css/estilos.css" rel="stylesheet">
  </head>
  <body>
    <!-- Fixed navbar -->
    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header"> 
            <a class="navbar-brand" href="http://www.univalle.edu.co" target="_blank">
                <img width="36" height="43" src="img/logo_univalle.gif">
            </a>
          <a class="navbar-brand">PLN <br>EISC</a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li <?php if (!isset($_GET['type']) || $_GET['type'] == 'linea_base') { echo 'class="active"'; } ?>><a href="index.php?type=linea_base">Analizador L&iacute;nea Base</a></li>
            <li <?php if (isset($_GET['type']) && $_GET['type'] == 'svm') { echo 'class="active"'; } ?>><a href="index.php?type=svm">Analizador con SVM</a></li>
            <li><a href="../AM/index.php">Analizador Morfologico </a></li>
           <li><a href="../NER/index.php">NER Freeling </a></li>
           <li><a href="about.php">Acerca de </a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>

    <div class="container theme-showcase">
        
      <!-- Main jumbotron for a primary marketing message or call to action -->
      <div class="jumbotron">
        <p>Analizador Sint&aacute;ctico Probabil&iacute;stico 
        <?php if (isset($_GET['type']) && $_GET['type'] == 'svm') { ?>
            con clasificaci&oacute;n de argumentos de verbo usando SVM
        <?php } ?>.</p>
        
        <form class="form-horizontal" role="form" action="analizar.php" method="post">
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">Oraci&oacute;n</label>
              <div class="col-sm-10">
                  <textarea class="form-control" rows="5" name="strEntrada" placeholder="Escriba la oración a ser analizada"></textarea>
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-default">Analizar</button>
                <input type="hidden" name="type" value="<?php echo (isset($_GET['type'])) ? $_GET['type'] : 'linea_base' ?>" />
              </div>
            </div>
          </form>
      </div>
    </div> 
    <!-- /container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>

