<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="John A. Vargas <javargas@gmail.com>">
    <title>Analizador Sintactico</title>
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/bootstrap-theme.min.css" rel="stylesheet">
    <link href="css/estilos.css" rel="stylesheet">
  </head>
  <body>
    <!-- Fixed navbar -->
    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="http://www.univalle.edu.co">
                <img width="36" height="43" src="http://www.univalle.edu.co/Id_Corporativa/Pics/logo.gif">
            </a>
          <a class="navbar-brand">PLN <br>EISC</a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li <?php if ($_POST['type'] == 'linea_base') { echo 'class="active"'; } ?>><a href="index.php?type=linea_base">Analizador L&iacute;nea Base</a></li>
            <li <?php if ($_POST['type'] == 'svm') { echo 'class="active"'; } ?>><a href="index.php?type=svm">Analizador con SVM</a></li>
            <li><a href="../AM/index.php">Analizador Morfologico </a></li>
	   <li><a href="../NER/index.php">NER Freeling </a></li>
           <li><a href="about.php">Acerca de </a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>

    <div class="container theme-showcase">
    <h1>Resumen</h1>
    
    <div class="jumbotron">
    El análisis sintáctico parcial hoy en día ha sido utilizado en las tareas de extracción de información y minería de textos, específicamente, los analizadores sintácticos probabilísticos
los cuales permiten la obtención de subestructuras y estructuras del árbol más posible de
una sentencia dada. El análisis sintáctico parcial se caracteriza en predecir la estructura de
frase en forma de árbol para una sentencia dada en lenguaje natural. Este tipo de análisis
se realiza bajo el enfoque basado en corpus donde este es el insumo anotado manualmente
con estructuras sintacticas. Un algoritmo de aprendizaje de maquinas suministra el modelo
de aprendizaje sobre el corpus y anota automaticamante la estructura sintactica de una sentencia
de entrada. Un analizador sintactico parcial puede ser denido como un analizador
sintactico probabilstico el cual consiste de un corpus anotado sintacticamente, un modelo
probablstico para el manejo de las dependencia entre variables y un enfoque de aprendizaje
supervisado para la obtencion del modelo de aprendizaje. Varios son los analizadores
sintacticos probabilsticos que son usados en las tareas principales del procesamiento de lenguaje
natural (PLN). Una de las tareas principales de estos analizadores probabilsticos tiene
que ver con la prediccion de estructuras sintacticas para tareas de extraccion avanzadas. En
el momento, el rendimiento de estos analizadores depende de los corpus y de las capacidad
de las modelos para cada lenguaje, por ejemplo, para el ingles el rendimiento esta entre el
80% y 90% mientras que para el espa~nol esta en el 85 %. La capacidad de los modelos a
su vez depende de la variabilidad y uso de las caractersticas para reponder a los fenomenos
lingusticos de cada lengua. Se han realizado muchos trabajos que aportan a la precision en la
inferencia de estas estructuras de frase basados en tecnicas probabilsticas. Michael Collins
propone tres modelos probabilsticos; el primero basado en reglas de gramaticas libres de
contextos probabilsticas lexicalizadas, donde la tarea es encontrar el nucleo sintactico de la
frase. En el segundo modelo adiciona un componente para las distincion entre adjuncion y
complementacion, y en el tercer modelo integra rastros de movimientos del nucleo sintactico
y adiciona la caracterstica de diferencia en los nodos no-terminales. Mas adelante, Dan Bikel
propone una metodologa para la investigacion de modelos probabilsticos que abordan el
problema del analisis sintactico.
En este trabajo se uso la implementacion de Bikel con el n de estudiar los modelos de
analisis sintacticos probabilsticos, se denieron caractersticas lingusticas para usar tecnicas
de maquinas de soporte vectorial y poder obtener una clasicacion de los complementos
que son requeridos como argumentos de los verbos. As como lo hace el segundo modelo de
Collins, se usa esta clasicacion para ser contrastada con los tipos de complementos que son
requeridos por el nucleo en caso de tratarse de un verbo. Los tipos de complementos requeridos
como argumentos se obtienen de la informacion lexica suministrada por ANCORA, que
provee un archivo para cada lema de los verbos existentes en el idioma espa~nol. Se realizaron
pruebas de validacion cruzada con 610 archivos del subconjunto de ANCORA CESS ESP, presentando F1 score para el modelo de lnea base 􀀀;􀀀 y del 􀀀;􀀀 para el modelo con
clasicacion de argumentos, de esta manera se logra una mejora del 􀀀;􀀀%. Este trabajo
presenta un analizador sintactico probabilstico entrenado por el corpus del espa~nol ANCORA
y un modelo de ajuste del analizador de Bikel usando maquinas de soporte vectorial
para clasicar el tipo de complemento de los modicadores de los verbos que son argumentos.
    </div>
    </div> <!-- /container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>

