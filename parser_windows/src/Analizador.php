<?php
ini_set('display_errors', 'On');
error_reporting(E_ALL);

define( 'LOG_PHRASE', 0 );
define( 'LOG_LANG', 1 );
define('PATH_JAVA', '"c:\Program Files\Java\jre7\bin\java"');

require_once( 'lib/class.template.php' );
require_once( 'src/counter.php' );
require_once( 'src/log.php' );
require_once( 'src/lang.php' );

/**
 * Clase encargada de la interfaz entre php y java
 *
 * @author John A. Vargas <javargas@gmail.com>
 */
class Analizador {
    private static $archivoEntrada = 'tmp/archivoEntrada';
    public static $archivoPosTag = 'tmp/archivoPosTag';
    
    /**
     * Genera el archivo de entrada para el parser de Bikel.
     * 
     * @param type $strEntrada
     */
    public static function generarArchivoEntrada($strEntrada)
    {
       self::$archivoEntrada .= time(); 
       $fp = fopen(self::$archivoEntrada, "w+");
       fwrite($fp, $strEntrada);
       fclose($fp);
    }
    
    /**
     * Ejecución del programa Freeling para posTagging
     */
    public static function posTagging()
    {
        $cmd = "analyzer -f config\\es.cfg < " . self::$archivoEntrada;
        //$cmd = "/usr/local/bin/analyze -f /usr/local/share/freeling/config/es.cfg < " . self::$archivoEntrada . ' 2>&1';
        //echo $cmd;
        
        $shellOutput = shell_exec($cmd);
        //echo "<pre>"; print_r($shellOutput); echo "</pre>";
        
        $lineas = explode("\n", $shellOutput);
        $cadenaPosTag = '(';
        
        foreach ($lineas as $linea) {
            $linea = trim($linea);
            
            if ($linea != '') {
                $palabras = explode(" ", $linea);
                $cadenaPosTag .= ' ('.$palabras[0].' ('.strtolower($palabras[2]).')) ';
            }
        }
        
        $cadenaPosTag .= ')';
        
       self::$archivoPosTag .= time(); 
       $fp = fopen(self::$archivoPosTag, "w+");
       fwrite($fp, $cadenaPosTag);
       fclose($fp);
    }
    	
    /**
     * Ejecuta el parser de Bikel
     */
    public static function ejecutarBikel()
    {
        /* Comando para ejecutar java */
        //$cmd = '"C:\Program Files\Java\jre7\bin\java" -server -Xms500m -Xmx500m ';        
        $cmd = PATH_JAVA . ' -server -Xms500m -Xmx500m ';
        
        /* Ruta para encontrar las clases */
        $cmd .= " -classpath lib/dbparser.jar;lib/AnalizadorSintactico.jar "; // Windows
        //$cmd .= ' -classpath lib/dbparser.jar:lib/AnalizadorSintactico.jar '; // Linux
        
        /* Configuracion */
        $cmd .= ' -Dparser.settingsFile=settings/spanish.properties';
        
        /* Clase Principal */
        $cmd .= ' danbikel.parser.Parser ';
        
        /* Archivo de entrenamiento */
        //$cmd .= ' -is obj/104_c-1.obj.gz';   // => Un archivo de entrenamiento     
        $cmd .= ' -is obj/datos_entrenamiento.obj.gz'; // => Dos archivos de entrenamiento
        
        /* Archivo de entrada */
        $cmd .= ' -sa '.self::$archivoPosTag . ' > salida_parser_bikel ';//' 2>&1';

        //echo $cmd;
        $shellOutput = shell_exec($cmd);
        //print "The Shell Output is : <pre>" ; print_r($shellOutput);print "</pre><br />";      
    }    
        
    /**
     * Ejecuta el parser de Bikel
     */
    public static function ejecutarBikelSVM()
    {    
        /* Comando para ejecutar java */
        //$cmd = '"C:\Program Files\Java\jre7\bin\java" -server -Xms500m -Xmx500m ';        
        $cmd = PATH_JAVA . ' -server -Xms500m -Xmx500m ';
        
        /* Ruta para encontrar las clases */
        $cmd .= ' -classpath lib/Bikelparser.jar;lib/AnalizadorSintactico.jar;lib/libsvmjar '; // Windows
        //$cmd .= ' -classpath lib/Bikelparser.jar:lib/libsvm.jar:lib/AnalizadorSintactico.jar '; // Linux
        
        /* Configuracion */
        $cmd .= ' -Dparser.settingsFile=settings/spanish.properties';
        
        /* Clase Principal */
        $cmd .= ' danbikel.parser.Parser ';
        
        /* Archivo de entrenamiento */
        //$cmd .= ' -is obj/104_c-1.obj.gz';   // => Un archivo de entrenamiento     
        $cmd .= ' -is obj/datos_entrenamiento.obj.gz'; // => Dos archivos de entrenamiento
        
        /* Archivo de entrada */
        $cmd .= ' -sa '.self::$archivoPosTag . ' > salida_parser &';//' 2>&1';

        //echo $cmd; 
        $shellOutput = shell_exec($cmd);
        //print "The Shell Output is : <pre>" ; print_r($shellOutput);print "</pre><br />";      
    }

    public static function dibujarArbol($archivoTag = null)
    {        
        if ($archivoTag == null) $archivoTag = self::$archivoPosTag.'.parsed';
    
        $data = trim( file_get_contents ($archivoTag) );
        
        
        $data = str_replace('(', '[', $data);
        
        $_SESSION['data'] = htmlentities(str_replace(')', ']', $data), ENT_IGNORE, 'UTF-8');
        
        $_SESSION['color']     = true; 
        $_SESSION['antialias'] = true;
        $_SESSION['autosub']   = true;
        $_SESSION['triangles'] = true;

        $font = 'lsansuni.ttf';
        $font = 'VeraSe.ttf';
        $_SESSION['font']    = $font;

        $_SESSION['fontsize'] = 10;

        // Increment phrase counter (displayed on the bottom of the page)
        AddCounter();

        //echo "<pre>"; print_r($_SESSION); echo "</pre>";
        include('stgraph.svg.php');
    }
}
?>
