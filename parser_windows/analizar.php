<?php
/**
 * Este archivo es el controlador
 * 
 * Se encarga de despachar la solicitud realizada por el formulario
 * para analizar sintacticamente un texto
 */
 
//ignore_user_abort(1); // sigue ejecutando aunque el usuario halla cerrado la conexión 
set_time_limit(0); // el script no tiene límite de tiempo 
 
require_once("src/Analizador.php");
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="John A. Vargas <javargas@gmail.com>">
    <title>Analizador Sintactico</title>
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/bootstrap-theme.min.css" rel="stylesheet">
    <link href="css/estilos.css" rel="stylesheet">
  </head>
  <body>
    <!-- Fixed navbar -->
    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="http://www.univalle.edu.co" target="_blank">
                <img width="36" height="43" src="img/logo_univalle.gif">
            </a>
          <a class="navbar-brand">PLN <br>EISC</a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li <?php if ($_POST['type'] == 'linea_base') { echo 'class="active"'; } ?>><a href="index.php?type=linea_base">Analizador L&iacute;nea Base</a></li>
            <li <?php if ($_POST['type'] == 'svm') { echo 'class="active"'; } ?>><a href="index.php?type=svm">Analizador con SVM</a></li>
            <li><a href="../AM/index.php">Analizador Morfologico </a></li>
	   <li><a href="../NER/index.php">NER Freeling </a></li>
           <li><a href="about.php">Acerca de </a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>

    <div class="container theme-showcase">
    
      <!-- Main jumbotron for a primary marketing message or call to action -->
      <div class="jumbotron">
        <p>Analizador Sint&aacute;ctico Probabil&iacute;stico
        <?php if (isset($_POST['type']) && $_POST['type'] == 'svm') { ?>
            con clasificaci&oacute;n de argumentos de verbo usando SVM
        <?php } ?>.</p>
        
        <form class="form-horizontal" role="form" action="analizar.php" method="post">
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">Oraci&oacute;n</label>
              <div class="col-sm-10">
                  <textarea class="form-control" rows="3" name="strEntrada" placeholder="Escriba la oración a ser analizada"><?php echo $_POST['strEntrada'] ?></textarea>
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-default">Analizar</button>
                <input type="hidden" name="type" value="<?php echo (isset($_POST['type'])) ? $_POST['type'] : 'linea_base' ?>" />
              </div>
            </div>
          </form>
      </div>
    
    <?php
    if (isset($_POST['strEntrada']) && !empty($_POST['strEntrada'])) {

        exec("rm -rf tmp/*");
        
        echo '<b>Cadena de entrada:</b><pre>';
        echo $_POST['strEntrada'];
        echo '</pre>';
        
        Analizador::generarArchivoEntrada($_POST['strEntrada']);
        
        Analizador::posTagging();

        echo "<b>Cadena con tags (Freeling)</b><pre>";
        echo file_get_contents(Analizador::$archivoPosTag);
        echo "</pre>";

        if ($_POST['type'] == 'svm') {
            Analizador::ejecutarBikelSVM();
            
            echo '<div id="idLogSVM"></div>
                  <script>setInterval(function(){ mostrarLogSVM(); }, 1000);</script>';
            
        } else {
            Analizador::ejecutarBikel();
            
            echo '<div id="idLogSVM"></div>
                  <script>setInterval(function(){ mostrarLog_bikel(); }, 1000);</script>';
        }
    }
    ?>

    </div> <!-- /container -->

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script>
        var continuarMostrandoLog = true;
    
        function mostrarLogSVM()
        {
            if (!continuarMostrandoLog) return;
        
            $.ajax({
              url: "mostrar_log.php"
            }).done(function(strHTML) {
                $('#idLogSVM').html(strHTML);
               
                var strEOF = /EOF/;
                if (strEOF.test(strHTML)) {
                    continuarMostrandoLog = false;
                    mostrarResultadoBikelSVM('<?php echo Analizador::$archivoPosTag ?>.parsed');
                }               
            });
        }
        
        function mostrarResultadoBikelSVM(archivoTag)
        {
            $.ajax({
              url: "mostrar_resultadoBikelSVM.php?archivoTag=" + archivoTag
            }).done(function(strHTML) {
                $('#idLogSVM').html(strHTML);
            });
        }
        
    
        function mostrarLog_bikel()
        {
            if (!continuarMostrandoLog) return;
        
            $.ajax({
              url: "mostrar_log_bikel.php"
            }).done(function(strHTML) {
                $('#idLogSVM').html(strHTML);
               
                var strEOF = /EOF/;
                if (strEOF.test(strHTML)) {
                    continuarMostrandoLog = false;
                    mostrarResultadoBikel('<?php echo Analizador::$archivoPosTag ?>.parsed');
                }               
            });
        }
        
        function mostrarResultadoBikel(archivoTag)
        {
            $.ajax({
              url: "mostrar_resultadoBikelSVM.php?archivoTag=" + archivoTag
            }).done(function(strHTML) {
                $('#idLogSVM').html(strHTML);
            });
        }
    </script>
  </body>
</html>

