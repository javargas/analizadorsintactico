<?php
/**
 * Este archivo es el controlador
 * 
 * Se encarga de despachar la solicitud realizada por el formulario
 * para analizar sintacticamente un texto
 */
require_once("src/Analizador.php");
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="John A. Vargas <javargas@gmail.com>">
    <title>Analizador Sintactico</title>
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/bootstrap-theme.min.css" rel="stylesheet">
    <link href="css/estilos.css" rel="stylesheet">
  </head>
  <body>
    <!-- Fixed navbar -->
    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <a class="navbar-brand">Analizador Sint&aacute;ctico</a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li><a href="index.php">Principal</a></li>
            <li class="active"><a href="#">Resultado</a></li>
            <li><a href="#about">Acerca de</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>

    <div class="container theme-showcase">
    <?php
    if (isset($_POST['strEntrada']) && !empty($_POST['strEntrada'])) {

        echo '<b>Cadena de entrada:</b><pre>';
        echo $_POST['strEntrada'];
        echo '</pre>';
        
        Analizador::generarArchivoEntrada($_POST['strEntrada']);
        
        Analizador::posTagging();

        echo "<b>Cadena con tags (Freeling)</b><pre>";
        echo file_get_contents(Analizador::$archivoPosTag);
        echo "</pre>";

        Analizador::ejecutarBikel();

        echo "<b>Cadena de s&aacute;lida (&Aacute;rbol en formato TBF)</b><pre>";
        echo file_get_contents (Analizador::$archivoPosTag.'.parsed');  
        echo "</pre>";

        echo '<b>&Aacute;rbol de An&aacute;lisis Sint&aacute;ctico</b><div class="jumbotron">';
        Analizador::dibujarArbol();
        echo '</div>';
    }
    ?>

    </div> <!-- /container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="../../dist/js/bootstrap.min.js"></script>
    <script src="../../docs-assets/js/holder.js"></script>
  </body>
</html>

