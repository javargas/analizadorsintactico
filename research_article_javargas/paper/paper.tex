\documentclass{llncs}
\usepackage{epsf}
\usepackage[utf8]{inputenc}
\usepackage[spanish]{babel}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{epic}
\usepackage{eepic}
\input{ps-fig.tex}
\newcommand\norm[1]{\left\lVert#1\right\rVert}

\begin{document}

\title{Analizador sintáctico probabilístico con clasificación de argumentos de verbo para el idioma español}

\author{
	Raul Gutierrez de Piñeres Ph.d. and John Alexander Vargas}
\institute{Univerisdad del Valle\\
	Escuela de Ingeniería de Sistemas y Computación\\
	Cali, Colombia\\
	raul.gutierrez@correounivalle.edu.co,\\
	john.a.vargas@correounivalle.edu.co}

\maketitle

\begin{abstract}	
El análisis sintáctico parcial hoy en día ha sido utilizado en las tareas de extracción de información y minería de textos, específicamente, los analizadores sintácticos probabilísticos los cuales permiten la obtención de subestructuras y estructuras del árbol más posible de una sentencia dada. El análisis sintáctico parcial se caracteriza en predecir la estructura de frase en forma de árbol para una sentencia dada en lenguaje natural. Este tipo de análisis se realiza bajo el enfoque de manejo de los datos (data-driven) sobre un corpus de entrenamiento que tiene las estructuras sintácticas de los casos de uso de un lenguaje en particular.

Un algoritmo de aprendizaje de máquinas suministra el modelo de aprendizaje sobre el corpus y anota automáticamante la estructura sintáctica de una sentencia de entrada. Un analizador sintáctico parcial puede ser definido como un analizador sintáctico probabilístico el cual consiste de un corpus anotado sintácticamente, un modelo probablístico para el manejo de las dependencia entre variables y un enfoque de aprendizaje supervisado para la obtención del modelo de aprendizaje.
Varios son los analizadores sintácticos probabilísticos que son usados
en las tareas principales del procesamiento de lenguaje natural (PLN). Una
de las tareas principales de los analizadores probabilísticos tiene que ver con la predicción de estructuras sintácticas para tareas de extracción avanzadas.

En el momento, el rendimiento de estos analizadores dependen del corpus
y de las capacidad de las modelos para cada lenguaje, por ejemplo, para el
inglés el rendimiento está entre el 80\% y 90\% mientras que para el español está entre 66\% - 85\%. La capacidad de los modelos a su vez depende de la variabilidad y uso de las características para reponder a los fenómenos lingüísticos de cada lengua. Se han realizado muchos trabajos que aportan a la precisión en la inferencia de estas estructuras de frase basados en técnicas probabilísticas.
Michael Collins propone tres modelos probabilísticos; el primero basado en reglas de gramáticas libres de contextos probabilísticas lexicalizadas, donde la tarea es encontrar el núcleo sintáctico de la frase. En el segundo modelo adiciona un componente para las distinción entre adjunción y complementación, y en el tercer modelo integra rastros de movimientos del núcleo sintáctico y adiciona la característica de diferencia en los nodos no-terminales. Mas adelante, Dan Bikel propone una metodología para la investigación de modelos probabilísticos que abordan el problema del análisis sintáctico.\\

Este trabajo define una metodología para la definición de un analizador sintáctico usando el algoritmo de Bikel sobre el corpus de entrenamiento ANCORA. Se establecen un conjunto de características lingüísticas y se utiliza un clasificador de complementos para el mejoramiento y anotación explícita  del analizador sintáctico. Así como lo hace el segundo modelo de Collins, se usa esta clasificación para ser contrastada con los tipos de complementos que son requeridos por el núcleo en caso de tratarse de un verbo. Los tipos de complementos requeridos como argumentos se obtienen de la información léxica suministrada por ANCORA, que provee un archivo para cada lema de los verbos existentes en el idioma español. A diferencia del trabajo de Collins, este trabajo al igual que el de Chuprala \cite{Chrupala2006} busca optimizar el rendimiento del analizador probabilístico usando SVMs sin modificar el modelo probabilístico interno  de clasificación. Collins y Bikel establecieron en este modelo interno el cual es inalterable para este trabajo. Se persigue la misma funcionalidad del modelo 2 de Collins usando la clasificación de argumentos desde la extracción y entrenamiento de los argumentos bajo el enfoque de manejo de datos de ANCORA.\\

Finalmente, se realizaron pruebas de validación cruzada con $ 610 $ archivos del subconjunto de ANCORA CESS\_ESP, presentando un precisión tanto para el modelo de línea base como para el modelo con clasificación de argumentos del $ 63 \% $. En esta dirección, en Chuprala \cite{Chrupala2006}, Cowan \cite{Cowan2005} se establece una precisión del $ 66,67 \% $ sobre la salida del parser mejorando el modelo de línea base en un $ 6,74 \% $, usando SVMs, el algoritmo de Bikel y un conjunto más robusto de reglas de cabeza que el definido en esta tesis. Este trabajo presenta un analizador sintáctico probabilístico  en el cual la mejora del modelo baseline se representa por la anotación explícita de los argumentos y no por la precisión del analizador.  El analizador es entrenado sobre el corpus del español ANCORA y un modelo de ajuste del analizador de Bikel es desarrollado usando máquinas de soporte vectorial para clasificar los argumentos de los verbos como complementos o adjuntos.
\end{abstract}

\section{Introducción}
Los analizadores sintácticos probabilísticos hoy en día son una de las técnicas del procesamiento del lenguaje natural más usadas en tareas como, renombramiento de entidades, sistemas para resúmenes, sistemas de pregunta-respuesta, social media, etc. El uso de estos analizadores está comprobado por la precisión al momento de anotar cada una de las sentencias que son testeadas. En este sentido, existen trabajos estructurados como el de Charniak \cite{Charniak97}, Bikel \cite{Bikel04} y Collins \cite{Collins03} que se constituyen en pilares de los analizadores en la cual las estructuras y subestructuras de un lenguaje son extraidas. El análisis sintáctico de una oración escrita en lenguaje natural consiste en recuperar la estructura sintáctica o árbol sintáctico asociados a esa oración. Para ello se utiliza un gramática que describe la estructura sintáctica del lenguaje. Un algoritmo determina cuál es el árbol sintáctico  de la oración y en la medida que la oración no pertenezca al lenguaje  no se proporciona ninguna solución.  En Allen \cite{allen1995}  se puede encontrar una descripción de los algoritmos clásicos que realizan \textit{análisis sintáctico completo}. Uno de los trabajos más importantes sobre analizadores probabílisticos es el de Michael Collins \cite{Collins03} el cual desarrolló tres modelos probabilísticos para hacer análisis sintáctico a través de gramáticas libres de contexto probabilísticas, de los tres modelos, en el primero se basó en reglas heurísticas para encontrar el núcleo sintáctico en una grupo sintáctico y una medida de distancia para definir la relación entre dos palabras, el segundo modelo se basó en reglas para distinguir entre adjuntos y complementos. El tercer modelo se basa en los rastros de movimiento de los núcleos. Dan Bikel implementó el modelo de Collins construyendo un motor multi-lenguaje para análisis sintáctico probabilístico. \\

Este documento presenta el desarrollo de un analizador sintáctico probabilístico para el español que usa el modelo computacional de Collins-Bikel en el cual se implementa un clasificador de modificadores de adjunción y modificadores complemento usando técnicas de máquinas de soporte vectorial. Este clasificador sirve para la extracción y etiquetado de las características de adjunción y complemento que no están definidas en el analizador de Bikel.  En esta trabajo se desarrollaron los paquetes necesarios para su configuración al idioma español, se construyen marcos de subcategorización a partir del conjunto de lemas del corpus ANCORA, y se define un clasificador de argumentos de verbo entre complementos y adjuntos  para clasificar y verificar que los modificadores de los verbos encontrados en una oración, correspondan con los argumentos requeridos según ANCORA, así como lo hace el segundo modelo de Collins.

\section{Analizador sintáctico probabilístico}
El análisis sintáctico consiste en encontrar la estructura de árbol de una sentencia dada, de acuerdo a una gramática definida. Uno de los principales problemas de este proceso es la ambigüedad, dado que para una misma frase se puede tener más de un árbol sintáctico. Existen trabajos como el análisis sintáctico probabilístico que intentan resolver este problema a través de un modelo probabilístico usando como datos de entrenamiento el corpus ANCORA.

\subsection{Modelo de Collins}
Michael Collins  propone un modelo de análisis sintáctico probabilístico, el cual es un modelo generativo de las gramáticas libres de conexto probabilísticas lexicalizadas. Collins extiende el modelo incluyendo un tratamiento probabilístico de subcategorización y traza del movimiento del núcleo sintáctico. El primer modelo adiciona el concepto de distancia, extendiendo el modelo basado en historias. En general, a través de cada modificador se puede depender de una función $\phi$ de previos modificadores, categorías núcleo/padre y palabras-núcleo.

\begin{center}
	$P_{l}(L_{i}(l_{i})|L_{1}(l_{1})\dots L_{i-1}(l_{i-1}),P(h),H)=P_{l}(L_{i}(l_{i})|\phi(L_{1}(l_{1})\dots L_{i-1}(l_{i-1}),P(h),H))$
	\par
\end{center}

\begin{center}
	$P_{r}(R_{j}(r_{j})|L_{1}(l_{1})\dots L_{n+1}(l_{n+1}),R_{1}(r_{1})\dots R_{j-1}(r_{j-1}),P(h),H)=P_{r}(R_{j}(r_{j})|\phi(L_{1}(l_{1})\dots L_{n+1}(l_{n+1}),R_{1}(r_{1})\dots R_{j-1}(r_{j-1}),P(h),H))$
	\par
\end{center}

Asumiendo que los modificadores son generados independientemente,
la distancia puede ser incorporada en el modelo incrementando la cantidad de dependencias entre estos modificadores. Si el orden de derivación es corregido para ser primero en profundidad, el modelo puede condicionar en cualquier estructura abajo de modificadores:

\begin{center}
	$P_{l}(L_{i}(l_{i})|H,P,h,L_{1}(l_{1})\dots L_{i-1}(l_{i-1}))=P_{l}(L_{i}(l_{i})|H,P,h,distance_{l}(i-1))$
	\par
\end{center}

\begin{center}
	$P_{r}(R_{i}(r_{i})|H,P,h,R_{1}(r_{1})\dots R_{i-1}(r_{i-1}))=P_{l}(R_{i}(r_{i})|H,P,h,distance_{r}(i-1))$
	\par
\end{center}

donde $distance_{l}$ y $distance_{r}$ son funciones de la cadena
de superficie abajo de modificadores previos. La medida de distancia
es un vector con los 2 siguientes elementos: 

\begin{enumerate}
	\item La cadena es de longitud cero.
	\item La cadena contiene un verbo.
\end{enumerate}

\subsubsection{Modelo 2}

\begin{figure}
	\centering
	\includegraphics[scale=0.6]{img/ejemplo-complemento}
	\caption{Ejemplo de un árbol mostrando el complemento \cite{Collins03}}
	\label{fig:ejemplo_complemento_collins}
\end{figure}

El segundo modelo \cite{Collins97} corresponde a una distinción entre adjunción/complemento y subcategorización. Cuando se identifican los complementos se marcan con el sufijo ``-C'' en los no-terminales. En la figura \ref{fig:ejemplo_complemento_collins} se muestra un ejemplo teniendo a ``IBM'' como complemento y a ``Last week'' como un adjunto. La etapa de preprocesamiento puede adicionar este detalle a la salida
del analizador, pero aún así hay dos buenas razones para hacer la
distinción:

\begin{enumerate}
	\item Identificar los complementos es suficientemente complejo para garantizar 	un tratamiento probabilístico. La información léxica es necesaria.  
	\item Hacer la distinción entre adjunción/complemento mientras se hace el 	análisis, puede ayudar a mejorar la precisión.
\end{enumerate}

El modelo puede aprender las propiedades lexicas disguiendolas entre complementos y adjuntos. Por lo tanto este puede sufrir de malos supuestos. El proceso generativo es extendido para incluir una escogencia probabilística de marcos de subcategorización del lado izquierdo, y del lado derecho del núcleo.

\begin{itemize}
	\item Escoger el núcleo (cabeza) $ H $ con probabilidad $ P_H(H | P, h) $.
	\item Escoger los marcos de subcategorización del lado izquierdo y el lado derecho del núcleo, LC y RC con probabilidades $ P_{LC}(LC | P, H, h) $ y $ P_{RC}(RC | P, H, h) $, Cada marco de subcategorización es un multiconjunto especificando los complementos que son requeridos por el núcleo en su lado izquierdo y derecho.
	\item Generar los modificadores del lado izquierdo y derecho con probabilidades 
	
	\begin{center}
		$ P_l(L_i, l_i | H, P, h, distancia(i - 1), LC) $ \\
		y \\
		$ P_r(R_i, r_i | H, P, h, distancia(i - 1), RC) $
	\end{center}
	
	respectivamente. Estos requerimientos de marcos de subcategorización son adicionados al contexto condicionado. Cuando los complementos son generados, estos son removidos del multiconjunto de subcategorizaciones.
\end{itemize}

\subsubsection{Modelo 3}
Otro obstáculo para extraer la estructura de argumentos-predicados de los árboles de análisis sintáctico es el movimiento del núcleo sintáctico. Las frases de nombre son extraidas de la posición de sujeto y la posición del objeto. Esto puede hacerse escribiendo patrones basados en reglas que identifican trazas en un árbol sintáctico. Esta tarea es también lo suficientemente compleja para garantizar un tratamiento probabilístico y la integración puede ayudar a mejorar la precisión del análisis sintáctico. Otra razón para un tratamiendo integrado de trazas es mejorar la parametrización del modelo. En particular, las probabilidades de subcategorización son marcadas por extracción. Dado que las LHS de la regla tiene una brecha (gap), hay tres formas de que esta brecha sea pasada hacia abajo a el RHS. \textbf{Head} el gap es pasado al núcleo de la frase, como una regla. \textbf{Left, Right} El gap es pasado recursivamente a uno de los modificadores de la izquierda o derecha del núcleo, o marcado como un argumento de traza a la izquierda o derecha del núcleo. Se especifica un parámetro $P_{G}(G|P,h,H)$ donde $G$ es otro  \textbf{Head}, \textbf{Left} o \textbf{Right}. El proceso generativo es extendido a escoger entre los casos despues de generar el núcleo sintáctico. El resto de la frase es generada en diferentes formas dependiendo de como el gap es propagado. En el caso del \textbf{Head} los modificadores de iquierda y derecha son generados como normal. En el caso de \textbf{Left, Right} un gap requerido es adicionado a otro de variable SUBCAT izquierda o derecha.

\subsection{Implementación de Bikel y Reglas de cabeza}
Dan Bikel en su tesis doctoral \cite{Bikel04} desarrolló técnicas y metodologías para examinar sistemas complejos como los modelos de análisis sintáctico probabilísticos lexicalizados. La primera idea es tratar el modelo como datos, el cual no es un método particular, pero es un paradigma y metodología de investigación. Para conseguirlo construye un motor de análisis sintáctico multi-lenguaje con la capacidad de instanciar una gran variedad de modelos analizadores probabilísticos. Como modelo línea base apropiado escoge instanciar los parámetros del segundo modelo de Collins. Collins define en su trabajo \cite{Collins03} reglas heurísticas para encontrar el núcleo sintáctico de una frase del idioma inglés basado en las reglas de cabeza de Magerman \cite{Magerman95}. Estas reglas no son aplicables al idioma español por las diferencias gramaticales que existen entre los dos idiomas, y se hace necesario crear reglas para buscar los núcleos de los componentes sintácticos o constituyentes según las reglas que usa el corpus Ancora. Basados en la reglas de Magerman Brooke Cowan en su trabajo de doctorado \cite{Cowan2008} define un conjunto de reglas para encontrar el núcleo sintáctico que son usadas para modelos de análisis sintáctico para el español como también lo hace Chrupala \cite{Chrupala2006}. Este conjunto de reglas determinísticas especifican cual de los hijos de un nodo padre es el núcleo. Las demás reglas se definen basadas en la gramática del idioma español.

\subsection{Ancora Corpus}
ANCORA (ANnotated CORpora) es un corpus del catalán (AnCOra-CA) y
español (AnCOra-ES) con diferentes niveles de anotación. Cada corpus
contiene 500.000 palabras que han sido construidas de manera incremental
a través de trabajos previos como el corpus 3LB: 3LB-CAT y 3LB-ESP,
cada uno con 100.000 palabras correspondientes a 4.000 oraciones para
el español y 2.600 oraciones para el catalán. Ambos corpus estan automáticamente etiquetados con información morfosintáctica y chequeada manualmente. Estos han sido ampliamente usados como corpus de entrenamiento para sistemas de aprendizaje, sistemas basados en reglas y sitemas de etiquetamiento (Pos Tagging). Los corpus 3LB son sintácticamente etiquetados con constituyentes y funciones de una manera manual. La información lingüística anotada en ANCORA se encuentra en formato XML y en formato TBF (Treebank Bracketted Format). Este último formato es propio para analizadores sintácticos, siendo XML un formato más abierto para otro tipo de información lingüística. AnCOra es el resultado de extender los corpus 3LB-CAT/ESP sobre 500.000 palabras y enriquecida con información semántica en diferentes niveles: estructuras de argumentos, roles temáticos, clases semánticas, entidades nombradas (NE) y sentidos nominales. Además de la anotación sintáctica y anotación manual semántica, el corpus de Ancora fué anotado automáticamente en su parte morfológica, que consitió en la asociación de un lema, categoría y atributos morfológicos a cada palabra del corpus. Este anotación morfológico implica analizar cada pieza del corpus y asignarle todas las posibles etiquetas que pueda recibir. En el proceso de desambigüación se selecciona solo una etiqueta. El conjunto de etiquetas usado por Ancora (corpus con árboles sintácticos) y Freeling (Herramienta de etiquetamiento de las palabras) es el propuesto por el grupo EAGLES para la anotación morfosináctica de lexicones y corpus para todas las lenguas europeas.

\section{Clasificador de argumentos de verbo}
Se define un clasificador de argumentos de verbo entre complementos y adjuntos, usando máquinas de vectores de soporte. Este clasificador proporciona información semántica en el mismo sentido que lo hace el segundo modelo de Collins basado en marcos de subcategorización. En este caso los marcos de subcategorización se contruyen a partir de la información suministrada por el corpus ANCORA sobre los argumentos requeridos por un verbo en particular. Los árboles de ANCORA proporcionan información sobre el tipo de argumentos de los sintagmas verbales, y de esta manera se construye el conjunto de entrenamiento requerido para la máquina de soporte vectorial. Usando la técnica de búsqueda exhaustiva se obtienen los valores para los parámetros usados por la SVM. Estos valores son $ C = 32, \gamma = 0,0078125 $ y una función kernel de base radial $ K(x, y) = e^{-\gamma \norm{x-y}^2} $

Con base en las características definidas en los trabajos presentados por Jurafsky, Pradham, Reyes y Chrupala, se definen a continuación las características para la implementación del clasificador de argumentos de verbo que es usado en el analizador sintáctico probabilístico de Bikel, como un refuerzo del segundo modelo de Collins. Estas características fueron escogidas primero por su facilidad de implementación y segundo porque se consideran relevantes para determinar si un item constituyente es un argumento verbal o no.

\begin{itemize}
	\item \textbf{Posición del núcleo}: Obtener la posición del núcleo sintáctico.
	\item \textbf{Codificación de etiqueta} Asignación de un código para la etiqueta del nodo raíz del árbol.
	\item \textbf{Aridad}: Número de hijos del nodo padre.
	\item \textbf{Anchura}: Número de nodos hojas.
	\item \textbf{Longitud}: Longitud del árbol.
	\item \textbf{Distancia}: Número de palabras entre el nodo del constituyente modificador y el verbo.
\end{itemize}

\section{Modificación del analizador sintáctico}	
 El modelo de Collins se basa en el algoritmo CKY, y en la implementación de Bikel se definen dos operaciones básicas para la construcción de los ítems. Que son \emph{addUnaries} (se crean producciones unarias)  y \emph{joinItems} (se crean producciones de binarias). Bikel también define un método \emph{isArgumentFast} que recibe la etiqueta del ítem para determinar si es o no un argumento. En este punto, se adiciona un llamado al método que determina el argumento a partir del ítem y no de la etiqueta raíz. Este proceso de análisis se modifica de la siguiente manera:

\begin{itemize}
	\item En la fase de entrenamiento se modificó el componente de entrenamiento de Bikel para generar los archivos de entrenamiento requeridos por el clasificador SVM. Estos archivos de entrenamiento son las características obtenidas de los ítems que hacen parte de los árboles de entrenamiento. ANCORA tiene anotado para los ítems de los árboles aquellos que son identificados como argumentos, y el tipo de argumentos que es  (suj, cd, ci, atr, cpred, creg, cag, cc). De esta manera se construyen los archivos de entrenamiento para el SVM.
	
	\item Dentro del proceso generativo en la fase de decodificación en el algoritmo CKY cuando se realiza la unión entre dos ítems, y uno de esos ítems es un verbo, se aprovecha el hecho de que ANCORA tiene anotado para cada verbo los argumentos requeridos, se obtienen estos argumentos y son adicionados a un conjunto de argumentos requeridos como lo hace el segundo modelo de Collins. El módulo de clasificación tiene un método que extrae las características lingüisticas del ítem (modificador del ítem con el verbo) y a través de libsvm se determina si es argumento y de serlo, que tipo de argumentos es, si es un adjunto (complementos circunstancial) o si es un complemento (directo, indirectio, etc). Se verifica entonces con el conjunto de argumentos requeridos obtenidos de ANCORA, y en el caso de que no pertenezca al conjunto no se ejecuta la unión de los ítems. En caso contrario, es decir en el que si existe al argumento dentro del conjunto de ANCORA, entonces se permite ejecutar la unión de los ítems.
\end{itemize}

\begin{figure}
	\centering
	\includegraphics[scale=0.775]{img/procesos_analizador_sintactico}
	\caption{Procesos del analizador sintáctico probabilístico}
	\label{fig:procesos_analizador}
\end{figure}

De esta manera se tiene la idea de que los árboles sintácticos generados cuentan con un mecanismo que proporciona un sentido semántico en cuanto a las relaciones de argumentos con respecto al verbo. En la figura \ref{fig:procesos_analizador} se muestran la arquitectura del analizador sintáctico probabilístico con la integración del clasificador de argumentos. El modelo del analizador es generado por el entrenamiento del algoritmo de Bikel bajo el modelo 2 de Collins. Por otra parte las características para el entrenamiento del SVM son extraidas desde ANCORA y se genera el modelo SVM, luego se clasifican los argumentos que alimentan a su vez al analizador pero no influyen en el modelo de Collins. Finalmente, se produce el árbol sintáctico de la sentencia de entrada anotados los argumentos de los verbos. Cabe anotar que Chrupala en \cite{Chrupala2006} define un clasificador de roles semánticos sobre los árboles generados por el algoritmo de Bikel para la asignación de ertiquetas funcionales. Esta clasificación se realiza a la salida producida por el analizador sintáctico, mientras que el clasificador de argumentos de verbo definido en este trabajo es usado junto con el segundo modelo de Collins para producir un árbol sintáctico con mayor precisión. Aunque ambos clasificadores lo hacen sobre árboles generados por el analizador y sobre el corpus de ANCORA, el momento de uso de cada uno es diferente.

\section{Evaluación}
Para la evaluación del clasificador de línea base fue utilizado, un conjunto de entrenamiento de 4.000 árboles sintácticos y un conjunto de testeo de 370 árboles particionados en 10 conjuntos sobre todo el corpus de ANCORA. Los árboles generados por el analizador son comparados con los árboles del corpus que han sido seleccionados como árboles meta (gold trees). Para la comparación de estos árboles, se usa el algoritmo parseval para obtener la precisión y cobertura de los árboles generados por el analizador sintáctico. En general, el desempeño del clasificador de Bikel sobre ANCORA tiene una precisión del $ 63\% $, cobertura del $ 62\% $ con un f-score del $ 63\% $ como lo muestra la tabla \ref{tbl:comparacion_metricas_bikel}. En este sentido, se puede explicar que para conjuntos de oraciones de más de 40 palabras la precisión  sufre una baja debido a que el número de constituyentes de $T$ (árbol analizado manualmente por lingüístas), es mucho mayor que el número de constituyentes correctos en $P$ (árbol analizado automáticamente por el sistema). De igual forma la cobertura aumenta porque el número de constituyentes correctos en $T$ disminuye con relación al número de constituyentes correctos en $P$. Por lo tanto, se puede deducir que para oraciones mayores de 40 palabras el analizador debe inferir un conjunto mayor de reglas de cabeza.  Esto también porque la estructura interna del analizador cambia en el sentido de los adjuntos y complementos, pero no se ve reflejado en la anotación explícita. Comparando estos resultados con los realizados en trabajos anteriores como el de Chrupala \cite{Chrupala2006} se puede observar que el f-score baja, primero porque el conjunto de reglas de cabeza es menor y también porque la estructura que modifica el modelo 2 de Collins es una estrucutra LFG en la cual la forma de encontrar el núcleo es diferente a las reglas de Magerman \cite{Magerman95} usadas en este trabajo. En esta dirección Chrupala define un corpus y un conjunto de reglas para la generación de los árboles sintácticos. Siendo una diferencia marcada con uno de los objetivos de este trabajo que era el de anotar explícitamente los adjuntos y complementos.

Al igual que para el entrenamiento del analizador de Bikel se definió un conjunto de entrenamiento de 4.000 árboles sintácticos y un conjunto de testeo de 370 árboles particionados en 10 conjuntos sobre todo el corpus de ANCORA. En general, la precisión del analizador fue la misma que la del modelo de línea base porque el trabajo estuvo enfocado en la consolidación del modelo 2 Collins para la generación de estructuras sintácticas. En este sentido, se comprueba que la precisión del modelo base de Bikel y el modelo con clasificador de argumentos tienen la misma precisión y cobertura sobre el mismo conjunto de entrenamiento. Con respecto al mejoramiento del analizador base se implementó una estructura que permite la anotación explícita de los adjuntos y complementos pero que no fue evaluada. Lo anterior quiere decir que se fija una estructura sintáctica-semántica sin todavia probar pero que tiene una precisión igual al modelo base (Bikel). El clasificador de argumentos es un aporte al trabajo realizado sobre analizadores sintácticos porque provee una estructura semántica de características lo cual no garantiza inicialmente el mejoramiento de los analizadores. Se espera que en otras pruebas con la anotación explícita de los complementos y adjuntos se puedan evidenciar muestras de mejoramiento del modelo de Bikel.  

\begin{center}
	\begin{table}
		\centering	
		\begin{tabular}{|l|c|c|c|}
			\hline 
			& Presición & Cobertura & Medida F \\
			\hline 
			Chrupala Baseline & 73,95 & 70,67 & 72,27 \\
			\hline 
			Chrupala SVM & 76,90 & 74,48 & 75,67 \\
			\hline 
			Baseline & 63,33 & 62,38 & 62,83 \\
			\hline
			SVM & 63,33 & 62,38 & 62,83 \\
			\hline
		\end{tabular}
		
		\caption{Evaluación de resultados del analizador Baseline y SVM}
		\label{tbl:comparacion_metricas_bikel}
	\end{table}
\end{center}

\section{Conclusiones y trabajo futuro}
El segundo modelo de Collins se basa en identificar argumentos para eliminar los requeridos dado los marcos de subcategorización que son obtenidos a través de un tratamiento probabilístico a partir del corpus ANCORA. Tener más efectividad en esta identificación supone una mejora en la precisión del análisis sintáctico. En este trabajo se adicionó información semántica en la identificación de argumentos del verbo y una mejora en el mecanismo de clasificación con técnicas que han mostrado éxito en esta tarea como son las máquinas de vectores de soporte y que resulta ser una alternativa viable en este aspecto, pero en este caso a parte de usar el tratamiento probabilístico de Collins para crear los marcos de subcategorización se ha usado la información que provee el corpus de ANCORA para los lemas de lo verbos sobre los tipos de argumentos necesarios y el requerimiento es eliminado cuando el clasificador por máquinas de vectores  lo identifica y clasifica como tal, pero no presenta mejora en la presición del árbol encontrado.

\subsection{Recomendaciones}

\begin{itemize}
	\item Definir más características léxicas, sintácticas, semánticas y contextuales para mejorar el componente clasificador basado en máquinas de vectores de soporte.
	\item Buscar la manera de optimizar el proceso de extracción de características.
	\item Utilizar técnicas de validación cruzada para encontrar un mejor ajuste en los parámetros $C$ y $\gamma$ del clasificador de argumentos de verbo.
	\item Definir un conjunto de pruebas con la anotación explicita de adjuntos y complementos de verbos.
	\end{itemize}

\bibliographystyle{splncs}
\bibliography{paper}

\end{document}
