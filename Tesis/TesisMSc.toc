\contentsline {chapter}{\hbox to\@tempdima {\hfil }Resumen}{\es@scroman {v}}
\select@language {spanish}
\contentsline {chapter}{\numberline {1}Introducci\IeC {\'o}n}{2}
\contentsline {section}{\numberline {1.1}Problema}{3}
\contentsline {section}{\numberline {1.2}Objetivos}{4}
\contentsline {subsection}{\numberline {1.2.1}Objetivo General}{4}
\contentsline {subsection}{\numberline {1.2.2}Objetivos Espec\IeC {\'\i }ficos}{4}
\contentsline {chapter}{\numberline {2}Marco Te\IeC {\'o}rico}{5}
\contentsline {section}{\numberline {2.1}An\IeC {\'a}lisis Sint\IeC {\'a}ctico}{5}
\contentsline {subsection}{\numberline {2.1.1}Gram\IeC {\'a}ticas Libres de Contexto (CFG: Context Free Grammar)}{6}
\contentsline {subsection}{\numberline {2.1.2}Gram\IeC {\'a}ticas de Adjunci\IeC {\'o}n \IeC {\'a}rboles (TAG)}{6}
\contentsline {subsection}{\numberline {2.1.3}\IeC {\'A}rboles de derivaci\IeC {\'o}n}{9}
\contentsline {section}{\numberline {2.2}Modelos probabil\IeC {\'\i }sticos}{9}
\contentsline {subsubsection}{\nonumberline Estimaci\IeC {\'o}n de m\IeC {\'a}xima similitud}{10}
\contentsline {subsubsection}{\nonumberline Gram\IeC {\'a}ticas Probabil\IeC {\'\i }sticas Libres de Contexto (PCFG)}{10}
\contentsline {subsubsection}{\nonumberline Definici\IeC {\'o}n Formal}{11}
\contentsline {subsubsection}{\nonumberline PCFG Lexicalizadas}{11}
\contentsline {subsubsection}{\nonumberline Modelos basados en historias}{11}
\contentsline {subsection}{\numberline {2.2.1}Modelo de Collins}{12}
\contentsline {subsubsection}{\nonumberline Modelo B\IeC {\'a}sico}{12}
\contentsline {subsubsection}{\nonumberline Modelo 1}{12}
\contentsline {subsubsection}{\nonumberline Modelo 2}{13}
\contentsline {subsubsection}{\nonumberline Modelo 3}{14}
\contentsline {subsection}{\numberline {2.2.2}Implementaci\IeC {\'o}n de Bikel}{15}
\contentsline {subsubsection}{\nonumberline Reglas para encontrar el n\IeC {\'u}cleo sint\IeC {\'a}ctico}{16}
\contentsline {section}{\numberline {2.3}Tecnolog\IeC {\'\i }as de NLP usadas}{17}
\contentsline {subsection}{\numberline {2.3.1}Ancora Corpus}{17}
\contentsline {subsection}{\numberline {2.3.2}Freeling}{20}
\contentsline {section}{\numberline {2.4}M\IeC {\'a}quinas de Vectores de Soporte}{21}
\contentsline {subsubsection}{\nonumberline Margen amplio}{21}
\contentsline {subsubsection}{\nonumberline El Kernel}{22}
\contentsline {section}{\numberline {2.5}Clasificaci\IeC {\'o}n de argumentos de verbo}{22}
\contentsline {subsection}{\numberline {2.5.1}Clasificaci\IeC {\'o}n de Roles sem\IeC {\'a}nticos}{24}
\contentsline {chapter}{\numberline {3}Clasificador de argumentos de verbos}{26}
\contentsline {subsection}{\numberline {3.0.2}Caracter\IeC {\'\i }sticas de los argumentos de verbo}{26}
\contentsline {section}{\numberline {3.1}Ajuste de par\IeC {\'a}metros}{27}
\contentsline {section}{\numberline {3.2}El algoritmo CKY y el modelo 2 de Collins}{28}
\contentsline {section}{\numberline {3.3}Modificaci\IeC {\'o}n del analizador sint\IeC {\'a}ctico}{29}
\contentsline {chapter}{\numberline {4}Evaluaci\IeC {\'o}n de los analizadores sint\IeC {\'a}cticos}{33}
\contentsline {section}{\numberline {4.1}Evaluaci\IeC {\'o}n del modelo l\IeC {\'\i }nea base}{34}
\contentsline {subsection}{\numberline {4.1.1}An\IeC {\'a}lisis de resultados}{34}
\contentsline {section}{\numberline {4.2}Ajustes de ejemplos para el clasificador de argumentos de verbo}{35}
\contentsline {section}{\numberline {4.3}Evaluaci\IeC {\'o}n del analizador sint\IeC {\'a}ctico con clasificaci\IeC {\'o}n de argumentos de verbo}{36}
\contentsline {subsection}{\numberline {4.3.1}An\IeC {\'a}lisis de resultados}{38}
\contentsline {chapter}{\numberline {5}Conclusiones y recomendaciones}{39}
\contentsline {section}{\numberline {5.1}Conclusiones}{39}
\contentsline {section}{\numberline {5.2}Trabajo Futuro}{39}
\contentsline {chapter}{\hbox to\@tempdima {\hfil }Bibliograf\IeC {\'\i }a}{41}
