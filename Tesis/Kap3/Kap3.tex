\chapter{Clasificador de argumentos de verbos}
En esta sección se define el clasificador de argumentos de verbo usando Máquinas de vectores de soporte (SVMs). Los argumentos de verbo clasificados servirán de insumo para la anotación sintáctica de las sentencias de entrada. En esta parte son también descritas las características y la forma de selección basados en conceptos semánticos. El uso de clasificadores y estructuras lingüísticas complejas en la construcción de los analizadores sintácticos probabilísticos es una posibilidad intermedia para el mejoramiento y comprensión de los PCFGs lexicalizados. Trabajos como el de Pradhan \& Jurafsky \cite{Pradhan04} utilizan SVMs para clasificación automática de argumentos para hacer análisis semántico superficial.  En esta tesis se pretende el uso de los clasificadores de argumentos de verbos para la anotación sintáctica de una sentencia dada. También se pretende anotar la información lingüística en el árbol sintáctico de salida el cual se propaga a través del uso del clasificador de argumentos de verbo. En esta parte se definen las características que que son usadas para la implementación del clasificador de argumentos.

\subsection{Características de los argumentos de verbo}
Con base en las características definidas en los trabajos presentados por Jurafsky \cite{Jurafsky2000}, Pradham \cite{Pradhan04}, Reyes \cite{Reyes13} y Chrupala \cite{Chrupala2006}, se definen a continuación las características para la implementación del clasificador de argumentos de verbo que es usado en el analizador sintáctico probabilístico de Bikel \cite{Bikel04}, como un refuerzo del segundo modelo de Collins \cite{Collins97}. Estas características fueron escogidas primero por su facilidad de implementación y segundo porque se consideran relevantes para determinar si un ítem constituyente es un argumento verbal o no. Las características son obtenidas a partir de los subárboles que representan constituyentes y conforman el árbol sintáctico de la oración. Son obtenidas en el momento de unir el subárbol, con otro que hace referencia a un grupo verbal. Esto para poder identificar el nodo constituyente como argumento de verbo.

\begin{itemize}
	\item \textbf{Posición del núcleo}: Obtener la posición del núcleo sintáctico. Cada subárbol representa un sintagma o constituyente de la oración, para obtener esta característica se implementó una función que determina de izquiera a derecha empezando en 1, la posición del núcleo del sintagma representado.
	\item \textbf{Codificación de etiqueta} Asignación de un código para la etiqueta del nodo raíz del árbol. Las etiquetas del nodo raíz son codificadas y convertidas a su representación binaria, como una distinción característica del subárbol. 
	\item \textbf{Aridad}: Número de hijos del nodo padre. Se refiere a la anchura del segundo nivel del árbol.
	\item \textbf{Anchura}: Número de nodos hojas. Se refiere al número de palabras que conforman el constituyente sintáctico.
	\item \textbf{Longitud}: Longitud del árbol. Corresponde al máximo número de nodos que existen entre el nodo raíz y una hoja del árbol. Se calcula la cantidad de nodos desde la raíz a las hojas del cual es seleccionado el mayor valor.
	\item \textbf{Distancia}: Número de palabras entre el nodo del constituyente modificador y el verbo. 
\end{itemize}

\section{Ajuste de parámetros}
Para este trabajo se decidió usar la librería \emph{libsvm} provista en \cite{Chang2011} para la implementación del algoritmo de clasificación con máquinas de vectores de soporte. Esta implementación requiere el ajuste para encontrar los mejores parámetros $C$ y $\gamma$, y la escogencia del tipo de función kernel para poder hacer el entrenamiento del clasificador. Para esto se sigue el procedimiento recomendado por Chih-Wei \cite{Hsu10apractical} que consiste en:

\begin{enumerate}
	\item Transformar los datos a formato SVM para lo cual se hacen conversiones en representaciones binarias de las características definidas.
	\item Se usa como kernel la función de base radial $ K(x, y) = e^{-\gamma \norm{x-y}^2} $
	\item Se usa búsqueda exhaustiva \emph{(Grid Search)} para encontrar los mejores parámetros $ C $ y $ \gamma $. Se obtienen los valores $C = 32$ y $\gamma = 0,0078125$.
	\item Se usan estos parámetros para entrenar en base al conjunto de características obtenidas en el proceso de entrenamiento del analizador al corpus de ANCORA.
\end{enumerate}

A continuación se describe la integración del algoritmo de máquinas de vectores de soporte para clasificación de argumentos de verbo, con el analizador sintáctico probabilístico implementado por Bikel adaptado para el idioma español. Inicialmente se hace una introducción del segundo modelo de Collins y luego se explica la modificación de este modelo usando el clasificador de argumentos de verbo.

\section{El algoritmo CKY y el modelo 2 de Collins}
En el segundo modelo de Collins un árbol sintáctico es representado por $m$ probabilidades de subcategorización, en adición a las $n$ dependencias definidias, las probabilidades de subcategorización corresponden a eventos de la forma \emph{``?`Cúal es la probabilidad de necesitar $n$ complementos de tipo $sn$ al lado derecho?''}. Donde $sn$ corresponde al no-terminal de un modificador complemento de verbo. El segundo modelo de Collins se puede describir de la siguiente manera:

\begin{itemize}
	\item Escoger el núcleo (cabeza) $H$ con probabilidad $P_H(H | P, h)$.
	\item Escoger los marcos de subcategorización del lado izquierdo y el lado derecho del núcleo, LC y RC con probabilidades $P_{LC}(LC | P, H, h)$ y $P_{RC}(RC | P, H, h)$, cada marco de subcategorización es un multiconjunto especificando los complementos que son requeridos por el núcleo en su lado izquierdo y derecho.
	\item Generar los modificadores del lado izquierdo y derecho con probabilidades 
	
	\begin{center}
		$ P_l(L_i, l_i | H, P, h, distancia(i - 1), LC) $ \\
		y \\
		$ P_r(R_i, r_i | H, P, h, distancia(i - 1), RC) $
	\end{center}
	
	Estos requerimientos de marcos de subcategorización son adicionados al contexto condicionado. Cuando los complementos son generados, estos son removidos del multiconjunto de subcategorizaciones requeridas.
\end{itemize}

En la implementación de Bikel, se ve reflejado en dos partes:

\begin{enumerate}
	\item En la fase de entrenamiento realiza una identificación de argumentos en complementos y adjuntos a partir de la información que provee ANCORA en los árboles sintácticos. En esta fase se genera un modelo de marcos de subcategorización basado en probabilidades calculadas a partir del corpus. Estas probabilidades son obtenidas en la fase de procesamiento y quedan registradas en el modelo probabilístico.
	
	\item En la fase de análisis o decodificación usa el modelo de marcos de subcategorización creado en el entrenamiento para generar conjuntos de argumentos requeridos cuando intenta unir dos items o subárboles en el algoritmo CKY \cite{CKY}, al momento de unir los ítems, se verifica si un ítem es argumento (complemento/adjunto). Si el ítem es identificado como un argumento se remueve el requerimiento que ha sido adicionado como resultado de calcular su probabilidad basado en el modelo de subcategorización creado en la fase de entrenamiento. para saber si es identificador como argumento se usa el método \emph{isArgumentFast} que debe ser implementado en la clase \emph{Training} del paquete del  idioma.\\
\end{enumerate}

\section{Modificación del analizador sintáctico}
La integración del clasificador de argumentos de verbo con el segundo modelo de Collins puede ser escrita como:

\begin{itemize}
	\item Sea $\alpha\in N$,
	\item $COMPLEMENTS(v)$ el conjunto de complementos de $v$, clasificados por el SVM, donde $v$ es un nodo del tipo grup.verb, por lo tanto $v=H$
	\item $P_{Comp}(\alpha)$ es una función tal que, 
	\item $P_{Comp}(\alpha)=\begin{cases}
	1 & si\,\alpha\in COMPLEMENTS(v)\\
	0 & en\, caso\, contrario
	\end{cases}$
	\item Entonces se modifica el algoritmo para que  $P_{lc}(LC|P,H,h)=P_{lc}(LC|P,H,h)P_{Comp}(LC)$	
\end{itemize}
	
La modificación del analizador sintáctico modelo 2 de Collins consiste en la integración del clasificador de argumentos al modelo multilenguaje de Bikel. Se persigue mejorar el rendimiento de este analizador y anotar explícitamente los argumentos de verbo que aparecen en la sentencia de entrada. La modificación se realiza en el algoritmo de Bikel en el cual se identifican los complementos y adjuntos usando el clasificador. Para efectuar este cambio se definen los ítems modificando que hace referencia al ítem del verbo, y el modificador el cual clasifica si es un argumento de verbo complemento o adjunto. Luego al momento de hacer la unión de los dos ítems, si el ítem modificando es un verbo, entonces a los ítems modificadores generados se les aplica la función de extracción de características para clasificar que tipo de complementos son.

\begin{center}
	$ \overrightarrow{fL_i} = extraer\_caracteristicas(L_i) $\\
	y\\		
	$ \overrightarrow{fR_i} = extraer\_caracteristicas(L_i) $
\end{center}

Donde $ \overrightarrow{fL_i} $ y $ \overrightarrow{fR_i} $ son los vectores de características lingüisticas binarizadas de los ítems seleccionados bajo el modelo probabilístico $ L_i $ y $ R_i $ respectivamente. El componente SVM clasifica el ítem con características $ \overrightarrow{fL_i} $ y $ \overrightarrow{fR_i} $ en uno de estos complementos: (suj, cd, ci, atr, cpred, creg, cag, cc). Se contrasta el tipo de complemento obtenido con los tipos de complementos requeridos por el núcleo según la información del lema del verbo en el corpus. El modelo de Collins se basa en el algoritmo CKY, y en la implementación de Bikel se definen dos operaciones básicas para la construcción de los ítems. Que son \emph{addUnaries} (se crean producciones unarias) y \emph{joinItems} (se crean producciones de binarias).\\

Bikel también define un método \emph{isArgumentFast} que recibe la etiqueta del ítem para determinar si es o no un argumento. Se adiciona un llamado al método que determina el argumento a partir del ítem y no de la etiqueta raíz. Este proceso de análisis se modifica de la siguiente manera:

\begin{itemize}
	\item En la fase de entrenamiento se modificó el componente de entrenamiento de Bikel para generar los archivos de entrenamiento requeridos por el clasificador SVM. Estos archivos de entrenamiento son las características obtenidas de los ítems que hacen parte de los árboles de entrenamiento. ANCORA tiene anotado para los ítems de los árboles aquellos que son identificados como argumentos, y el tipo de argumentos que es  (suj, cd, ci, atr, cpred, creg, cag, cc). De esta manera se construyen los archivos de entrenamiento para el SVM.
	
	\item Dentro del proceso generativo en la fase de decodificación en el algoritmo CKY cuando se realiza la unión entre dos ítems, y uno de esos ítems es un verbo, se aprovecha el hecho de que ANCORA tiene anotado para cada verbo los argumentos requeridos, se obtienen estos argumentos y son adicionados a un conjunto de argumentos requeridos como lo hace el segundo modelo de Collins. El módulo de clasificación tiene un método que extrae las características lingüisticas del ítem (modificador del ítem con el verbo) y a través de libsvm se determina si es argumento y de serlo, que tipo de argumentos es, si es un adjunto (complementos circunstancial) o si es un complemento (directo, indirectio, etc). Se verifica entonces con el conjunto de argumentos requeridos obtenidos de ANCORA, y en el caso de que no pertenezca al conjunto no se ejecuta la unión de los ítems. En caso contrario, es decir en el que si existe al argumento dentro del conjunto de ANCORA, entonces se permite ejecutar la unión de los ítems.
\end{itemize}

De esta manera se tiene la idea de que los árboles sintácticos generados cuentan con un mecanismo que proporciona un sentido semántico en cuanto a las relaciones de argumentos con respecto al verbo. El modelo del analizador es generado por el entrenamiento del algoritmo de Bikel bajo el modelo 2 de Collins. Por otra parte las características para el entrenamiento del SVM son extraidas desde ANCORA y se genera el modelo SVM, luego se clasifican los argumentos que alimentan a su vez al analizador pero no influyen en el modelo de Collins. Finalmente, se produce el árbol sintáctico de la sentencia de entrada anotados los argumentos de los verbos. Cabe anotar que Chrupala en \cite{Chrupala2006} define un clasificador de roles semánticos sobre los árboles generados por el algoritmo de Bikel para la asignación de ertiquetas funcionales. Esta clasificación se realiza a la salida producida por el analizador sintáctico, mientras que el clasificador de argumentos de verbo definido en este trabajo es usado junto con el segundo modelo de Collins para producir un árbol sintáctico con mayor precisión. Aunque ambos clasificadores lo hacen sobre árboles generados por el analizador y sobre el corpus de ANCORA, el momento de uso de cada uno es diferente. En las figuras \ref{fig:diag_entrenamiento} y \ref{fig:diag_decodificacion} se muestran las entradas y salidas correspondientes para las fases de entrenamiento y decodificación.

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.475]{img/diag_entrenamiento}
	\caption{Entradas y salidas de la fase de entrenamiento}
	\label{fig:diag_entrenamiento}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.5]{img/diag_decodificacion}
	\caption{Entradas y salidas de la fase de decodificación}
	\label{fig:diag_decodificacion}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.55]{img/diag_joinItems}
	\caption{Diagrama de actividades del método \emph{joinItems} modificado.}
	\label{fig:diag_decodificacion}
\end{figure}
