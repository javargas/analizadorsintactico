#!/usr/bin/python

import sys, re

strPath = sys.argv[1]
strNombreArchivoFull = strPath.split('/')
strNombreArchivo = strNombreArchivoFull[1].split('.')

fo = open(strPath, "r+")
fs = open("textos/" + strNombreArchivo[0] + '.txt', "w+")
contenido = fo.read()
salida = ''
#print contenido

listaPalabras = re.findall( r'\(\w+(.*?)\s(.*?)\)', contenido)

for nodoPalabra in listaPalabras:
    #l = nodoPalabra.split(' ')
    print '*' + nodoPalabra[1] + '*'
    palabraStrip = nodoPalabra[1].strip();
    palabraSplit = palabraStrip.split(' ');
    #print palabraSplit
    
    if len(palabraSplit) > 2:
        salida += ' ' + palabraSplit[1]
    else:
    	if len(palabraSplit) == 2 and re.match(r'\w+', palabraSplit[0]):
    		salida += ' ' + palabraSplit[0]
    	else:
    		if len(palabraSplit) == 2 and (palabraSplit[0][0] == ',' or palabraSplit[0][0] == '.' or palabraSplit[0][0] == '"'):
        		salida += palabraSplit[0][0]


fs.write(salida)
fs.close()
fo.close()
