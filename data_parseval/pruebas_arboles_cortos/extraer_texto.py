#!/usr/bin/python

'''
Este script extrae el texto de los archivos tbf
proporcionados por el subconjunto CESS-ESP de ANCORA corpus.

@author John Vargas <javargas@gmail.com>
@date Nov 21 2014
'''

import sys, re

strPath = sys.argv[1]
strNombreArchivoFull = strPath.split('/')
strNombreArchivo = strNombreArchivoFull[1].split('.')

fo = open(strPath, "r+")
fs = open("textos_tageados_gold/" + strNombreArchivo[0] + '.txt', "w+")
contenido = fo.read()
salida = ''
salidaPrint = ''
#print contenido

listaPalabras = re.findall( r'\(\w+(.*?)\s(.*?)\)', contenido)

for nodoPalabra in listaPalabras:
    #l = nodoPalabra.split(' ')
    #print '*' + nodoPalabra[1] + '*'
    palabraStrip = nodoPalabra[1].strip();
    palabraSplit = palabraStrip.split(' ');
    #print palabraSplit
    
    if len(palabraSplit) > 2:
        salida += '('+palabraSplit[1]+' ('+palabraSplit[0][1:]+')) '
    else:
    	if len(palabraSplit) == 2 and re.match(r'\w+', palabraSplit[0]):
    		salida += ' ' + palabraSplit[0]
    	else:
    		if len(palabraSplit) == 2 and (palabraSplit[0][0] == ',' or palabraSplit[0][0] == '.' or palabraSplit[0][0] == '"'):
                    if (palabraSplit[0][0] == ','):
        		salida += '('+palabraSplit[0][0]+' (Fc))'
                    if (palabraSplit[0][0] == '.'):
        		salida += '('+palabraSplit[0][0]+' (Fp))'
                    if (palabraSplit[0][0] == '"'):
        		salida += '('+palabraSplit[0][0]+' (Fe))'
salida = '('+salida+')'        		
fs.write(salida)
fs.close()
fo.close()
