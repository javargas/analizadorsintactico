; Archivo con reglas para encontrar el nucleo sintactico de 
; un constituyente o sintagma (head)
; Reglas de cabeza para el idioma español
; de acuerdo a las constituyentes del corpus ANCORA
;
; Autor: John Vargas <javargas@gmail.com>
; Fecha: Abril 1 de 2014
;
(
 (S (r ao) (r aq) (l s) (r grup.verb) (r sn) (l gv ) (l infintiu ) (l gerundi) )
 (grup.verb (r infinitiu) (r gerundi) (r vmp) (r vsp) (r vap) (r vmi))
 (grup.nom (l n)  (l grup.nom)  (l p)  (l w)  (l z)  (l a)  (l v) 
		(l s.a) (l participi) (l participle) (l relative) (l sp) (l sn)
 )
 (sn (r grup.nom) (r grup) (l sn) (l s) (l relatiu) (l sp) (r n))
 (sp (l prep) (l sp))
 (sadv (l grup.adv) (l rg) (l p) (l sadv))
 (espec.fp (l da0mp0) (l da0fs0) (l di0ms0) (l da0fp0))
 (snd (l grup.nom))
 (sa (l grup.a))
 (sno (l grup.nom))
 (conj.subord (l cs))
 (prep (l sps00))
 (espec.fs (l da0fs0))
 (sn.x (l grup.nom))
 (espec.ms (l da0ms0))
 (espec.mp (l Z))
 (conj (l cs))
 (coord (l cc))
 (grup (r N) (r W) (r P) (r Z) (r aq) (r grup))
 (inc (l s))
 (infinitiu (l v))
 (interjeccio (l i))
 (neg (l rn))
 (prep (l sp))
 (sa (l ao) (l aq))
 (sbar (l cp))
 (* (r))
)
