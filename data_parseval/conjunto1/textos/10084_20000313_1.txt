 El Partido_Popular alcanz� la mayor�a absoluta en Madrid y obtuvo 19 diputados , dos m�s que en 1996, con el 52,52_por_ciento de los sufragios y 1.610.000 votos -Fpa- al 99,83_% escrutado, lo que supone el mejor resultado del partido en Madrid desde la instauraci�n de la democracia. Insuficiente fue el crecimiento del PSOE en la Comunidad_de_Madrid , que obtuvo 12 esca�os , uno m�s que en las generales de hace cuatro a�os , pese_a perder casi 30.000 votos y quedarse con algo m�s 1.008.000 y que no sirvi� para frenar la derrota global. IU se convirti� en la gran derrotada al perder 3 de sus representantes en el Parlamento y la mitad de los votos al pasar de 547.000 votos a apenas 279.000, mientras_que el GIL aparece como cuarta fuerza m�s votada , pese_a no obtener esca�os con algo m�s de 32.000 sufragios. Los esca�os obtenidos por el PP en Madrid suponen " el mejor resultado del partido en Madrid en 25 a�os de democracia", dijo a Efe el presidente del PP en la regi�n , P�o_Garc�a-Escudero, quien contrapuso el "" de los populares a la " profunda crisis en la que ha entrado el PSOE y toda la izquierda". Adem�s en las elecciones generales celebradas hoy el PP consolid� su entrada en las localidades del denominado " cintur�n rojo" del sur de Madrid , en el que comparte victorias y derrotas con el PSOE , que sin_embargo sigue siendo el partido m�s votado en el c�mputo global de esta zona. En las localidades del denominado " cintur�n rojo", el PSOE logr� hoy la victoria en Fuenlabrada, Legan�s y Getafe , aunque en este municipio por muy escaso margen de votos, mientras el PP gan� en Alcorc�n, M�stoles y Pinto. En otros dos municipios que forman_parte del �rea exterior de este " cintur�n rojo" , Humanes y Valdemoro, tambi�n logr� hoy la victoria la lista al Congreso_de_los_Diputados encabezada por Jos�_Mar�a_Aznar. En los municipios de la zona oeste , tradicional feudo popular, el PP logro sus m�s amplias mayor�as. As� en Majadahonda y Pozuelo los populares triplicaron los votos del PSOE. Resultados similares obtuvieron tambi�n en algunas localidades del norte , como San_Sebasti�n_de_los_Reyes donde lograron el 71_por_ciento de los sufragios o de la zona Este , como Coslada con el 72_por_ciento de los votos. En los peque�os pueblos el voto mayoritario tambi�n tuvo color popular como en La_Hiruela el m�s peque�o de la regi�n , con 28 electores , de los cuales m�s del 63_por_ciento dio su confianza al PP o en La_Acebeda donde casi el 90_por_ciento de sus 56 electores vot� al Partido_Popular.